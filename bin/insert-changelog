#!/bin/sh
#
# insert-changelog - Add a new revision to rpm-info.xml
#

. "$(dirname $0)/fdp-functions"

OUTPUT="${1:-${PWD}/$RPMINFO.clog$$}"


# Check whether a number input is valid
# Relying on global scope since this function isn't useful outside me
validnumber()
{
    case "$ROLE" in
	'doc' )
	    VALIDCHARS='[^0-9.]'
	    MSG="Valid characters are digits and '.' only -- please re-enter."
	    ;;
	'rpm' )
	    VALIDCHARS='[^0-9]'
	    MSG="Valid characters are digits only -- please re-enter."
	    ;;
	* )
	    echo "Role '$ROLE' is invalid, cowardly bail-out"
	    exit 1
	    ;;
    esac
    if test -z "`echo $NUMBER | grep $VALIDCHARS`"; then
	RETVAL=0
    else
	echo "$MSG"
	RETVAL=1
    fi
    return $RETVAL
}


echo -n "Change to [r]pm package or [d]ocument source? [r/D] "
while test -z "$ROLE" ; do
    read -s -n 1 R
    case "$R" in
	'r' | 'R' )
	    ROLE="rpm" ;;
	'd' | 'D' | '' )
	    ROLE="doc" ;;
    esac
done
echo $ROLE

if test $ROLE = "rpm"; then
    REVDATE=`date +"%a %b %d %Y"`
    ZEROREV=0
    NUMBER=1
    REVDESC="an integer release number"
elif test $ROLE = "doc"; then
    REVDATE=`date +"%Y-%m-%d"`
    ZEROREV=0.0                  # unlikely, but you never know
    REVDESC="a decimal version number"
else
    echo "This should never happen; aborting"
    exit 1
fi

LASTREV=$(get_latest_revision "$ROLE")
if [ -z "$LASTREV" ] || [ "$LASTREV" = "0" ]; then
# this is the first revision for this role
    LASTREV=$ZEROREV
fi

while test -z "$NUMBER" ; do
    echo -n "Enter $REVDESC greater than $LASTREV: "
    read NUMBER
    if validnumber ; then
    # Yes, it's perfectly safe to assume Python is present
	if test `python -c "from distutils import version; a=version.LooseVersion('$LASTREV') ; b=version.LooseVersion('$NUMBER'); print b>a"` = "False"; then
	    NUMBER=
	fi
    else
	NUMBER=
    fi
done

get_all_workers
echo -n "Enter number for responsible contributor: "
read PERSON
ID=$(get_worker_attribute "id" $PERSON)

echo "Enter a one-line description of the change."
echo "To add extra details, edit rpm-info.xml afterward."
read DETAILS

${XSLTPROC} --stringparam role "$ROLE" \
    --stringparam number "$NUMBER" \
    --stringparam person "$ID" \
    --stringparam date "$REVDATE" \
    --stringparam detail "$DETAILS" \
    ${FDPDIR}/docs-common/packaging/insert-changelog.xsl "$RPMINFO" | \
    ${XMLFORMAT} ${XMLFOPTS} > ${OUTPUT}
echo "New $RPMINFO written to $OUTPUT"
