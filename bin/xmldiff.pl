#!/usr/bin/perl
#
#  Copyright (c) 2002, DecisionSoft Limited All rights reserved.
#  Please see:
#  http://software.decisionsoft.com/licence.html
#  for more information.
#
# Modified for the Fedora Docs Project by Tommy.Reynolds@MegaCoder.com
#

#
# xmldiff: xmldiff program - uses xmlpp, which must be on the ${PATH}
#

#Change this if xmlpp is not in your current path
#for example: $XMLFORMAT = "./xmlpp";
# $XMLFORMAT = "xmlpp";

use Getopt::Std;

getopts('scupChi');

if ($opt_h || @ARGV != 2) {
	usage();
}

my $diffOpts;

my $outputFmt = $opt_u + $opt_c + $opt_s + $opt_p + $opt_C;
if( $outputFmt == 0 )	{
	$outputFmt = $opt_p = 1;
}
if( $outputFmt > 1 )	{
  print STDERR "Error: Only one mode may be specified\n";
  usage();
}

if( $opt_s )	{
	# Standard diff, no playing around
} elsif( $opt_c )	{
	# Plain context diff
	$diffOpts .= "-c ";
} elsif( $opt_u )	{
	# Plain unified diff
	$diffOpts .= "-u ";
} elsif( $opt_p )	{
	# Colorized unified diff
	# $diffOpts .= "-u ";
	$diffOpts .= "--new-line-format='[1m[33m+ %l\n[m' ";
	$diffOpts .= "--old-line-format='[1m[31m- %l\n[m' ";
	$diffOpts .= "--unchanged-line-format='[1m[30m  %l[m\n' ";
} elsif( $opt_C )	{
  $diffOpts .= "--changed-group-format='\n<<<<<<<<<<<<<<\n%<==============\n%>>>>>>>>>>>>>>>\n\n' ";
  $diffOpts .= "--new-line-format='+ %l\n' ";
  $diffOpts .= "--old-line-format='- %l\n' ";
  $diffOpts .= "--unchanged-line-format='  %l\n' ";
}

$XMLFORMAT = "xmlformat";

$file1 = "xmlppTEMP1.$$";
$file2 = "xmlppTEMP2.$$";

my $results = 0;
$results += system("$XMLFORMAT '$ARGV[0]' > $file1");
$results += system("$XMLFORMAT '$ARGV[1]' > $file2");

$results += system("/usr/bin/diff -bB $diffOpts $file1 $file2");


unlink($file1,$file2);

exit( $results );

sub usage {
  print STDERR <<EOF;
usage: $0 [ mode ] [ options ] oldfile.xml newfile.xml

mode must be one of:
  -p  coloured unified diff [default]
  -c  context diff
  -u  unified diff
  -s  standard diff output
  -C  vaguely CVS like unified diff

options:
  -t  split attributes - good for spotting changes in attributes
  -i  ignore element and attribute contents

EOF
  exit 1;
}
