# Comments are treated as CDATA and not touched.  It's best to set
# them out from other text if possible.  A doublespace is nice but
# code is still code, so it's not really that important. (Down, inner
# stickler...)
#

*DEFAULT
  format = block
  entry-break = 1
  element-break = 1
  exit-break = 1
  subindent = 2
  normalize = no
  wrap-length = 72

*DOCUMENT
  format = block
  wrap-length = 72
  element-break = 2

article book
  element-break = 2

articleinfo bookinfo
  normalize = yes

year holder
  entry-break = 0
  exit-break = 0
  normalize = yes

firstname surname othername
  entry-break = 0
  exit-break = 0
  normalize = yes
  
revnumber date authorinitials
  entry-break = 0
  exit-break = 0
  normalize = yes

revremark
  normalize = yes

section
  entry-break = 1
  normalize = yes

# "Normalize" means make smart whitespace decisions
para simpara example important note warning caution itemizedlist variablelist varlistentry
  normalize = yes

title titleabbrev
  entry-break = 0
  exit-break = 0
  normalize = yes

emphasis literal abbrev code
  format = inline

trademark
  format = inline

# Do not fubar <screen> or <programlisting>
screen programlisting
  format = verbatim

# <entry> is special because a linebreak has meaning, best leave the
# decisions up to the experts
entry
  format = verbatim

command application filename option userinput computeroutput replaceable
  format = inline

# The <primary> and <secondary> subelements of <indexterm> are still block
firstterm
  format = inline
  normalize = yes

indexterm
  format = block
  normalize = no

primary secondary
  format = block
  entry-break = 1
  exit-break = 1

varlistentry
  element-break = 1

term
  entry-break = 0
  exit-break = 0
 
menuchoice guilabel guimenu guisubmenu guimenuitem guibutton keycap
  format = inline

wordasword systemitem citetitle footnote email
  format = inline

acronym
  format = inline

# Make <ulink> and <xref> less goofy in their use of whitespace
ulink xref
  format = inline

# Cover OMF files
creator description format identifier language maintainer omf relation subject type
  format = block
  normalize = yes
  entry-break = 1
  exit-break = 1

# Cover rpm-info files
details rights version desc
  format = block
  entry-break = 0
  exit-break = 0
  normalize = yes

# new 4.4+ options
package
  format = inline
