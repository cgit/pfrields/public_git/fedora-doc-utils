<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: -->
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
 "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [

<!ENTITY % FDP-ENTITIES SYSTEM "entities/entities-zh_CN.ent">
%FDP-ENTITIES;

]>
<chapter id="ch-cvs">
  <title>CVS</title>
  <para>并发版本控制系统 (<application>CVS</application>) 提供了一个框架，使多个用户可以修改相同的一组文件。可以想象，如果用户们在同一个目录中修改文件，将产生极大的混乱。使用 <application>CVS</application>，用户们就可以安全地修改相同的一组文件了。<application>CVS</application> 保留文件的主备份，并且在一个中央仓库中记录谁在何时修改了哪些内容。如果出现了冲突，<application>CVS</application> 可以告诉用户。<application>CVS</application> 通常用在程序员间共享代码，但是在文档撰写中也很方便。</para>
  <indexterm>
    <primary>cvs</primary>
  </indexterm>
  <section id="sn-cvs-overview">
    <title>CVS 如何运作</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>CVS 如何运作</secondary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary>概述</secondary>
    </indexterm>
    <para>多数情况下，构成项目的文件集在 CVS 服务器上保存为 <firstterm>module</firstterm>(模块)。</para>
    <para>在使用 <application>CVS</application> 时，先将模块 <firstterm>checkout</firstterm>(检出)一份到本地文件系统中。修改一个或多个文件后，将它们 <firstterm>commit</firstterm>(提交)回中央 <application>CVS</application> 服务器。</para>
    <para>使用 <application>CVS</application> 时，不必获取许可或加锁就可以修改文件。<application>CVS</application> 的名字中，<wordasword>concurrent</wordasword>(并发)这个词来源于它允许不同的人编辑同一文件的不同部分这一能力。只要这些修改不重叠，<application>CVS</application> 就可以正确地记录修改。如果出现重复的修改，会被标识在文件中，作者必须自行解决它们。</para>
    <para>在提交修改时，只有服务器知道的文件才会得到提交。也就是说，如果您在本地工作目录中创建了一个文件，新文件不会自动传输到服务器。您必须 <firstterm>add</firstterm>(添加)文件到仓库中，然后再提交它。如果您要删除一个文件，必须说明要从 CVS 服务器的仓库中删除文件，然后将文件的删除提交。</para>
    <para>执行这些动作的命令请参阅 <xref linkend="sn-cvs-cvscommands"/>。</para>
    <para>从上次您从 CVS 获取文件，到打算提交某个修改，这段时间里如果有人修改过这个文件，<application>CVS</application> 将尝试将修改合并到 <application>CVS</application> 服务器上的复本中。如果您与他人修改的是文件的不同部分，提交也许不会有任何 <firstterm>conflict</firstterm>(冲突)问题。如果他人修改的正是您打算提交的部分，您将看到发生冲突的提示。这时，您必须 <firstterm>update</firstterm>(更新)文件。在修改文件前总是更新它是个好习惯。请参考 <xref linkend="sn-cvs-cvscommands-conflicts"/> 中解决冲突的步骤。</para>
  </section>
  <section id="sn-cvs-preparation">
    <title>为 CVS 作准备</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>为 cvs 作准备</secondary>
    </indexterm>
    <para>在使用 <application>CVS</application> 之前，您必须在 <application>CVS</application> 服务器上建立帐户。得到帐户后，就不必再执行这些操作了。</para>
    <section id="sn-cvs-rpm-check">
      <title>CVS 是否已安装在系统中</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>RPM 方式安装</secondary>
      </indexterm>
      <para>您必须安装 <application>CVS</application> 的 <abbrev>RPM</abbrev>。可以这样检查是否已经安装：</para>
      <screen><prompt>$</prompt> <command>rpm -q cvs</command> </screen>
      <para>如果您看到了类似 <computeroutput>cvs-1.11.19-1</computeroutput> 的输出，那么已经安装了。如果看到类似 <computeroutput>package cvs is not installed</computeroutput> 的输出，这意味着您需要安装它。如果您不知道怎么安装，请向管理员求助。</para>
    </section>
    <section id="sn-cvs-generate-keys">
      <title>生成 SSH 密钥</title>
      <indexterm>
        <primary>OpenSSH</primary>
        <secondary>认证密钥</secondary>
      </indexterm>
      <para>
        <application>CVS</application> 服务器使用 <application>SSH</application> 2 协议密钥来认证用户。因此，您需要生成一对密钥，才能得到 <application>CVS</application> 帐号。如果您已经有了 <application>SSH</application> <abbrev>DSA</abbrev> 密钥，就可以跳过这一步。</para>
      <tip>
        <title>技巧</title>
        <para>如果系统中有 <filename>~/.ssh/id_dsa.pub</filename> 这个文件，那么您已经有了 <abbrev>DSA</abbrev> 密钥。</para>
        <para>如果现有的 <abbrev>DSA</abbrev> 密钥不需要 <wordasword>passphrase</wordasword>(口令)，您最好尽快生成一个需要口令的密钥。</para>
      </tip>
      <para>按照下列步骤来生成 <application>SSH</application> 2 协议使用的 <abbrev>DSA</abbrev> 密钥。这是 <computeroutput>cvs.fedoraproject.org</computeroutput> <application>CVS</application> 帐号必需的。</para>
      <orderedlist>
        <indexterm>
          <primary>OpenSSH</primary>
          <secondary>
            <command>ssh-keygen</command>
          </secondary>
        </indexterm>
        <indexterm>
          <primary>
            <command>ssh-keygen</command>
          </primary>
        </indexterm>
        <listitem>
          <para>要生成 SSH2 协议的 <acronym>DSA</acronym> 密钥，在 shell 提示符下，输入命令：</para>
          <screen><prompt>$</prompt> <command>ssh-keygen -t dsa</command> </screen>
          <para>接受默认的文件位置 <filename>~/.ssh/id_dsa</filename>。您应当使用口令来加强密钥的安全。输入与帐号密码不同的口令，然后确认它，只要重新输入一次。</para>
        </listitem>
        <listitem>
          <para>输入下列命令，将新的密钥复制到正确的文件中：</para>
          <screen><prompt>$</prompt> <command>cat ~/.ssh/id_dsa.pub&gt;&gt;~/.ssh/authorized_keys</command> </screen>
          <note>
            <para>在按下 <guibutton>ENTER</guibutton>(回车)键之前仔细检查命令。如果 <filename>~/.ssh/authorized_keys</filename> 已经存在， <filename>~/.ssh/id_dsa.pub</filename> 的内容将被追加到 <filename>~/.ssh/authorized_keys</filename> 文件的结尾。</para>
          </note>
        </listitem>
        <listitem>
          <para>输入下列命令，修改 <filename>~/.ssh</filename> 目录和密钥文件的权限：</para>
          <screen><prompt>$</prompt> <command>chmod 700 ~/.ssh</command>
<prompt>$</prompt> <command>chmod 644 ~/.ssh/authorized_keys</command> </screen>
        </listitem>
      </orderedlist>
      <tip>
        <title>技巧</title>
        <para>您可以让系统记住口令，这样访问 <application>CVS</application> 服务器时就不必每次输入了。请参考 <application>ssh-add</application> 程序的文档。</para>
      </tip>
    </section>
  </section>
  <section id="sn-cvs-config">
    <title>配置 CVS 访问</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>配置 cvs 访问</secondary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary>CVSROOT</secondary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary>CVS_RSH</secondary>
    </indexterm>
    <indexterm>
      <primary>CVSROOT</primary>
    </indexterm>
    <indexterm>
      <primary>CVS_RSH</primary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary>
        <filename>.cvsrc</filename>
      </secondary>
    </indexterm>
    <indexterm>
      <primary>
        <filename>.cvsrc</filename>
      </primary>
    </indexterm>
    <section id="sn-cvs-config-cvsrc">
      <title>避免重复输入</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>避免重复输入</secondary>
      </indexterm>
      <para>许多 <application>CVS</application> 命令需要特定的命令行选项才能搭配使用。与其每次用到时输入这些选项，您可以将它们保存在文件里，让 <application>CVS</application> 每次执行前读取它。</para>
      <para>在个人目录中创建一个叫做 <filename>~/.cvsrc</filename> 的文件。它应当包含下列命令，每个命令占一行： <screen><computeroutput>cvs -z3
diff -uNp
rdiff -uNp
update -dP</computeroutput></screen></para>
    </section>
    <section id="sn-cvs-config-anon">
      <title>配置只读的 CVS 访问</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>配置只读访问</secondary>
      </indexterm>
      <indexterm>
        <primary>cvs</primary>
        <secondary>匿名访问</secondary>
      </indexterm>
      <para>如果您的目的是下载各篇 &FC; 文档并在自己的系统中编译，那么只读访问就够了。照做本节的指令，然后直接跳转到 <xref linkend="sn-cvs-cvscommands-co"/>。</para>
      <para>切换到将存放 <application>CVS</application> 中文件的目录，然后执行下面的命令：</para>
      <screen><prompt>$</prompt> <command>export CVSROOT=:pserver:anonymous@cvs.fedoraproject.org:/cvs/docs</command>
<prompt>$</prompt> <command>cvs login</command>
<prompt>$</prompt> <command>cvs checkout docs-common</command> <replaceable>module-name</replaceable>
<prompt>$</prompt> <command>cvs checkout</command> <replaceable>module-name</replaceable>
<prompt>$</prompt> <command>cd</command> <replaceable>module-name</replaceable></screen>
      <para>检出模块之后，<envar>CVSROOT</envar> 环境变量的设置就不再有用，因为正确的值已经保存在了本地工作目录的每个子目录中，文件 <filename>CVS/Root</filename> 里面。只要当前工作目录包含 <filename>CVS/</filename> 目录，<application>CVS</application> 程序就可以自动找到 &FC; 仓库。</para>
    </section>
    <section id="sn-cvs-config-author">
      <title>配置可读写的 CVS 访问</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>配置读写访问</secondary>
      </indexterm>
      <indexterm>
        <primary>cvs</primary>
        <secondary>作为作者的设置</secondary>
      </indexterm>
      <para>要创作新文档，或是修改已有的，必须得到 &FC; Docs <application>CVS</application> 仓库的完整读写权限。关于这个过程，请参考 <ulink url="http://fedoraproject.org/wiki/DocsProject/NewWriters"><filename>http://fedoraproject.org/wiki/DocsProject/NewWriters</filename></ulink> 页面。下面是大致的步骤：</para>
      <itemizedlist>
        <listitem>
          <para>订阅 <ulink url="https://listman.redhat.com/mailman/listinfo/fedora-docs-list"><filename>fedora-docs-list</filename></ulink> 邮件列表，这是项目主要的讨论区。</para>
        </listitem>
        <listitem>
          <para>创建一个 GNU Privacy Guard (<abbrev>GPG</abbrev>) 密钥来向其他人标识自己的身份。</para>
        </listitem>
        <listitem>
          <para>注册 <ulink url="http://bugzilla.redhat.com/"><filename>Bugzilla</filename></ulink> 帐号，如果以前没有注册过的话。<filename>Bugzilla</filename> 是跟踪问题、修改和项目更新的途径。</para>
        </listitem>
        <listitem>
          <para>向邮件列表发出 <ulink url="http://fedoraproject.org/wiki/DocsProject/SelfIntroduction">self introduction</ulink>(自我介绍)。</para>
        </listitem>
      </itemizedlist>
      <para>当您的 <ulink url="http://fedoraproject.org/wiki/DocsProject/SelfIntroduction">自我介绍</ulink> 被接受后，您将获得 <application>CVS</application> 访问权。</para>
      <para>每位作者，也包括您，在自我介绍被接纳之后，就使用独一无二的 <envar>$CVSROOT</envar> 来访问 <application>CVS</application> 仓库：</para>
      <screen><prompt>$</prompt> <command>export CVSROOT=:ext:</command> <replaceable>yourname</replaceable> <command>@cvs.fedoraproject.org:/cvs/docs</command>
<prompt>$</prompt> <command>export CVS_RSH=/usr/bin/ssh</command> </screen>
      <para>设置了 <envar>$CVSROOT</envar> 和 <envar>$CVS_RSH</envar> 环境变量后，就可以访问仓库了：</para>
      <screen><prompt>$</prompt> <command>cvs co -c</command> </screen>
      <para>您将被提示输入 <application>SSH</application> 密钥的口令。按下 <guibutton>ENTER</guibutton> 可以看到仓库中已有模块的列表。</para>
    </section>
  </section>
  <section id="sn-cvs-cvscommands">
    <title>基本的 CVS 命令</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>命令</secondary>
    </indexterm>
    <para>配置好系统的 CVS 访问之后，检出要处理的模块。</para>
    <tip>
      <title>技巧</title>
      <para>关于是否需要设置 <envar>$CVSROOT</envar> 环境变量，或命令行选项 <option>-m </option><replaceable>repository</replaceable>，应当看当前工作目录是否有一个 <filename>CVS/</filename> 子目录。</para>
      <para>如果有 <filename>CVS/</filename> 子目录，<application>CVS</application> 忽略任何 <envar>$CVSROOT</envar> 或 <option>-m</option> 命令行选项设置。</para>
    </tip>
    <section id="sn-cvs-cvscommands-co">
      <title>检出模块</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>检出模块</secondary>
      </indexterm>
      <para>您只需检出模块一次。当一个模块检出到本地之后，它就一直在那儿了。</para>
      <para>要检出一个模块，执行下面的命令：</para>
      <screen><prompt>$</prompt> <command>cvs co</command>  <replaceable>&lt;module-name&gt;</replaceable></screen>
      <para>例如，要检出 <computeroutput>example-tutorial</computeroutput> 模块，先进入工作目录，然后执行下面的命令：</para>
      <screen><prompt>$</prompt> <command>cvs co example-tutorial</command> </screen>
      <para>当前目录下将新建一个名字是 <filename>example-tutorial/</filename> 的子目录。</para>
      <para>如果检出模块时没有指定分支名称，默认就是 <application>CVS</application> 中模块的 <firstterm>HEAD</firstterm> 分支。</para>
      <section id="sn-cvs-cvscommands-co-branch">
        <title>检出模块的分支</title>
        <indexterm>
          <primary>cvs</primary>
          <secondary>检出模块</secondary>
          <tertiary>检出分支</tertiary>
        </indexterm>
        <para>可以将 <application>CVS</application> 分支视为一篇手册的某个版本，或是一个文件的不同版本。</para>
        <para>要检出模块的分支，运行下面的命令：</para>
        <screen><prompt>$</prompt> <command>cvs co</command> <option>-d</option>  <replaceable>&lt;directory&gt;</replaceable> <option>-r</option>  <replaceable>&lt;branchname&gt;</replaceable> <replaceable>&lt;module-name&gt;</replaceable></screen>
        <para>将创建名为 <replaceable>&lt;directory&gt;</replaceable> 的目录， 模块 <replaceable>&lt;module-name&gt;</replaceable> 的 <replaceable>&lt;branchname&gt;</replaceable> 分支的文件将被复制到这个目录中。</para>
        <para>例如，要检出 <computeroutput>mymodule</computeroutput> 模块的 BRANCH-VERSION-1.2 分支，运行命令：</para>
        <screen><prompt>$</prompt> <command>cvs co -d mymodule-1.2 -r BRANCH-VERSION-1.2 mymodule</command> </screen>
        <para>模块的 BRANCH-VERSION-1.2 分支将被检出到 <filename>mymodule-1.2</filename> 目录中。</para>
        <para>要查看文件有哪些分支和标记，运行命令：</para>
        <screen><prompt>$</prompt> <command>cvs status</command> <option>-v</option> <replaceable>&lt;filename&gt;</replaceable></screen>
        <para>例如，文件 <filename>foo.sgml</filename> 的状态如下：</para>
        <screen>
          <computeroutput>
===================================================================
File: foo.sgml    Status: Up-to-date

   Working revision:    1.47
   Repository revision: 1.47    /cvs/docs/custom-guide/rhl-cg-en.sgml,v
   Sticky Tag:          (none)
   Sticky Date:         (none)
   Sticky Options:      (none)

   Existing Tags:
        BRANCH-VERSION-1.2              (branch: 1.25.2)
</computeroutput>
        </screen>
        <para>只有在 <computeroutput>Existing Tags</computeroutput>(现有标记)中显示为"分支"的标记才可以作为分支检出。</para>
      </section>
    </section>
    <section id="sn-cvs-cvscommands-up">
      <title>更新文件</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>更新文件</secondary>
      </indexterm>
      <para>要获取模块中文件的最新版本，切换到包含模块文件的目录，执行命令：</para>
      <screen><prompt>$</prompt> <command>cvs update</command> </screen>
      <para>模块所有文件的最新版本将下载到本地。如果您看到了文件冲突提示，请参考 <xref linkend="sn-cvs-cvscommands-conflicts"/>。</para>
    </section>
    <section id="sn-cvs-cvscommands-commit">
      <title>提交文件</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>提交文件</secondary>
      </indexterm>
      <para>在本地修改模块的文件之后，将它们提交，以在 <application>CVS</application> 服务器上保存更改：</para>
      <screen><prompt>$</prompt> <command>cvs commit</command> <option>-m</option> &quot;<replaceable>some log message</replaceable>&quot; <replaceable>filename</replaceable></screen>
      <note>
        <para>如果您想用喜欢的文本编辑器来撰写日志信息，并且已经定义了 $VISUAL 或 $EDITOR 环境变量，就可以忽略 <userinput>-m &quot;some log message&quot;</userinput>。编辑缓冲区中会包含一些描述修改的注释，您在添加自己的文字时，不必删掉它们。</para>
      </note>
      <para>日志信息应当尽可能有描述性，这样您和共同作者都可以了解做过了哪些修改。如果日志消息是 <userinput>updated some files</userinput>(更新了一些文件)，那它无法描述修改了什么，将来对您毫无用处。如果您修正了一个错误，应当包含 <application>Bugzilla</application> 引用。</para>
      <para>
        <replaceable>&lt;filename&gt;</replaceable> 可以是一个文件名，一系列以空格分隔的文件名，或者一组以通配符指定的文件名，类似 <filename>*.png</filename> 或 <filename>foo-*.sgml</filename>。</para>
      <para>如果在 <command>commit</command> 命令中没有给出文件名或通配符，所有修改都会被提交到服务器。这个命令是递归的，模块中任何子目录的修改也都会被提交。不带文件名执行 <command>commit</command> 命令时要小心，因为您可能不记得改过了什么。</para>
      <para>如果您看到了文件冲突，请参考 <xref linkend="sn-cvs-cvscommands-conflicts"/>。</para>
    </section>
    <section id="sn-cvs-cvscommands-add">
      <title>添加文件</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>添加文件</secondary>
      </indexterm>
      <para>要向模块添加文件，在本地目录中创建这个文件，然后执行下面的命令：</para>
      <screen><prompt>$</prompt> <command>cvs add</command>  <replaceable>file-to-add</replaceable></screen>
      <para>添加文件之后，必须 <command>commit</command>(提交)添加的文件到服务器：</para>
      <screen><prompt>$</prompt> <command>cvs commit</command> <option>-m</option> &quot;<replaceable>some log message</replaceable>&quot; <replaceable>file-to-add</replaceable></screen>
    </section>
    <section id="sn-cvs-cvscommands-admin">
      <title>管理二进制文件</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>二进制文件</secondary>
      </indexterm>
      <para>通常存档的文件是简单的文本文件，但是有时也会存档二进制文件。<application>CVS</application> 程序可以识别大多数文件扩展名，类似 <filename>.png</filename> 或 <filename>.jpg</filename>，这样就不会做坏事。</para>
      <para>当文件被检出时，<application>cvs</application> 扫描它，查找特殊的关键字，类似 &quot;<computeroutput>$id:$</computeroutput>&quot;，然后替换为相应的值， 例如版本号。</para>
      <para>关键字替换通常会损坏二进制文件，因此如果 <application>CVS</application> 无法正确识别文件为二进制文件，必须关闭替换。标识文件为二进制就关闭了关键字替换，输入命令：</para>
      <screen width="60"> <prompt>$</prompt> <command>cvs</command> <option>admin</option> <option>-kk</option> <replaceable>filename</replaceable></screen>
      <para>注意在使用 <option>admin</option> 命令前，文件必须已被检入 <abbrev>CVS</abbrev> 仓库。这样不会出错，因为关键字扩展是在文件被检出并复制到本地目录时进行的，而不是提交到仓库中的时候。</para>
      <tip>
        <title>恢复二进制文件</title>
        <para>如果您将二进制文件检入仓库，发现它在检出时被损坏了，不要心急。只要运行上面的 <option>admin</option> 命令，再删掉本地文件，然后再次检出就可以了。</para>
      </tip>
    </section>
    <section id="sn-cvs-cvscommands-rm">
      <title>删除文件</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>删除文件</secondary>
      </indexterm>
      <para>如果模块不再需要某个文件，使用 <command>remove</command> 命令来删除它，然后用 <command>commit</command> 命令将删除提交到服务器。即使模块的当前版本删除了某个文件，这个文件的副本还会被保留在服务器上，可以随时用 <command>add</command> 命令恢复。</para>
      <screen><prompt>$</prompt> <command>cvs rm -f</command>  <replaceable>file-to-remove</replaceable></screen>
      <para>删除文件后，必须 <command>commit</command>(提交)删除：</para>
      <screen><prompt>$</prompt> <command>cvs commit</command> <option>-m</option>&quot;<replaceable>some log message</replaceable>&quot; <replaceable>file-to-remove</replaceable></screen>
      <para>在用 <command>commit</command> 命令提交删除时不能使用通配符。必须明确地指定文件名。</para>
      <para>如果需要重命名某个文件，最好在 <application>CVS</application> 服务器上重命名它，这样文件的历史可以保留下来。如果需要重命名文件，就向 <ulink url="mailto:cvsdocs-administrator@fedoraproject.org"><filename>cvsdocs-administrator@fedoraproject.org</filename></ulink> 发送一封邮件，写明要重命名的文件。</para>
    </section>
    <section id="sn-cvs-cvscommands-status">
      <title>文件的状态</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>文件的状态</secondary>
      </indexterm>
      <para>有时需要查看模块中文件的 <firstterm>status</firstterm>(状态)。执行命令：</para>
      <screen><prompt>$</prompt> <command>cvs status</command>  <replaceable>filename</replaceable></screen>
      <para>仓库文件的状态报告如下所示：</para>
      <variablelist>
        <varlistentry>
          <term>
            <computeroutput>Up-to-date</computeroutput>
          </term>
          <listitem>
            <para>您的文件版本正是 <application>CVS</application> 服务器上的最新版本。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>Locally Modified</computeroutput>
          </term>
          <listitem>
            <para>您已从服务器上更新了最新的版本，并且做了修改。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>Locally Added</computeroutput>
          </term>
          <listitem>
            <para>您使用 <command>cvs add</command> 命令添加了文件，但是尚未提交文件的添加。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>Locally Removed</computeroutput>
          </term>
          <listitem>
            <para>您使用 <command>cvs remove</command> 命令删除了文件，但是尚未提交文件的删除。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>Needs Checkout</computeroutput>
          </term>
          <listitem>
            <para>服务器上有新版本的文件，您需要更新。尽管状态中说，需要检出，实际的意思是，您需要用 <command>cvs update</command> 命令更新文件。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>Needs Patch</computeroutput>
          </term>
          <listitem>
            <para>本地文件需要打补丁以更新到服务器的最新版。执行 <command>cvs update</command> 命令来解决。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>Needs Merge</computeroutput>
          </term>
          <listitem>
            <para>服务器上有更新的版本，您的本地版本却做了未提交的修改。这种情况可能经常发生，如果您没有获取最新版本就进行修改的话。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>File had conflicts on merge</computeroutput>
          </term>
          <listitem>
            <para>类似 <computeroutput>Needs Merge</computeroutput>，但是当您执行 <command>cvs update</command> 命令时，将无法自动解决冲突。关于如何解决冲突请参考 <xref linkend="sn-cvs-cvscommands-conflicts"/>。</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            <computeroutput>Unknown</computeroutput>
          </term>
          <listitem>
            <para>
              <application>CVS</application> 服务器不知道文件的任何信息。它没有被本地添加或删除，也从未提交到服务器上。这种情况通常发生在不应被提交到 <application>CVS</application> 服务器的文件上，例如 <filename>generated-index.sgml</filename>，或者是想添加到仓库但尚未执行 <command>cvs add</command> 命令的文件。</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>
    <section id="sn-cvs-cvscommands-conflicts">
      <title>解决冲突</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>解决冲突</secondary>
      </indexterm>
      <para>如果您修改了一个文件，而另一个人修改了同样的区域并且先提交了，您在提交文件或更新本地目录时就会看到类似下面的消息：</para>
      <screen>
        <computeroutput>
RCS file: /cvs/docs/module-name/filename.sgml,v
retrieving revision 1.12
retrieving revision 1.13
Merging differences between 1.12 and 1.13 into filename.sgml
rcsmerge: warning: conflicts during merge
cvs server: conflicts found in filename.sgml
C filename.sgml
</computeroutput>
      </screen>
      <para>要解决冲突，打开文件，搜索 <computeroutput>&lt;&lt;&lt;&lt;&lt;&lt;&lt;</computeroutput> 然后判断哪个版本才是正确的。例如：</para>
      <screen>
        <computeroutput>
&lt;para&gt;
Some sentence.
&lt;&lt;&lt;&lt;&lt;&lt;&lt; filename.sgml
A sentence that was changed in the working copy.
=======
A same sentence that was changed differently and committed.
&gt;&gt;&gt;&gt;&gt;&gt;&gt; 1.13
&lt;/para&gt;
</computeroutput>
      </screen>
      <para>
        <computeroutput>&lt;&lt;&lt;&lt;&lt;&lt;&lt;</computeroutput> 和 <computeroutput>=======</computeroutput> 之间的内容是工作目录里原有的。 <computeroutput>=======</computeroutput> 和 <computeroutput>&gt;&gt;&gt;&gt;&gt;&gt;&gt;</computeroutput> 之间是服务器上的内容。</para>
      <para>修改文件，解决冲突，然后提交它。</para>
    </section>
    <section id="sn-cvs-cvscommands-summary">
      <title>小结</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>命令</secondary>
        <tertiary>小结</tertiary>
      </indexterm>
      <para>所有命令都假定您的当前目录是正确的。</para>
      <table frame="all" id="tb-cvs-basic-commands">
        <title>基本的 CVS 命令</title>
        <tgroup cols="2">
          <colspec colname="shortcut" colnum="1" colwidth="30"/>
          <colspec colname="description" colnum="2" colwidth="60"/>
          <thead>
            <row>
              <entry>命令</entry>
              <entry>描述</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry>
                <command>cvs checkout <replaceable>&lt;module-name&gt;</replaceable></command> 或 <command>cvs co <replaceable>&lt;module-name&gt;</replaceable></command></entry>
              <entry>创建一个名为 <replaceable>&lt;module-name&gt;</replaceable> 的目录， 将模块内容放在目录中</entry>
            </row>
            <row>
              <entry>
                <command>cvs co -d <replaceable>&lt;directory&gt;</replaceable> -r <replaceable>&lt;branchname&gt;</replaceable><replaceable>&lt;module-name&gt;</replaceable></command>
              </entry>
              <entry>创建 <replaceable>&lt;directory&gt;</replaceable> 目录，将 <replaceable>&lt;module-name&gt;</replaceable> 模块的 <replaceable>&lt;branchname&gt;</replaceable> 分支放在目录中。</entry>
            </row>
            <row>
              <entry>
                <command>cvs update</command> 或 <command>cvs up</command></entry>
              <entry>从服务器将文件更新到最新版</entry>
            </row>
            <row>
              <entry>
                <command>cvs add <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>将文件 "filename" 添加到 CVS 服务器</entry>
            </row>
            <row>
              <entry>
                <command>cvs commit -m "My message" <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>将本地计算机上的文件 <replaceable>&lt;filename&gt;</replaceable> 提交为最新版</entry>
            </row>
            <row>
              <entry>
                <command>cvs log <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>查看文件 <replaceable>&lt;filename&gt;</replaceable> 的提交信息</entry>
            </row>
            <row>
              <entry>
                <command>cvs status <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>查看文件的状态，类似 <computeroutput>Locally
        Modified</computeroutput></entry>
            </row>
            <row>
              <entry>
                <command>cvs status -v <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>查看文件现有的标签和分支</entry>
            </row>
            <row>
              <entry>
                <command>cvs diff <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>查看文件的工作版本与分支中的最新版本的不同之处</entry>
            </row>
            <row>
              <entry>
                <command>cvs diff -r1.1 -r1.2 <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>查看文件 1.1 和 1.2 版本的不同之处</entry>
            </row>
          </tbody>
        </tgroup>
      </table>
      <para>请阅读系统中的 CVS 手册来获取更多信息，位置是 <filename>/usr/share/doc/cvs-<replaceable>&lt;version-number&gt;</replaceable>/cvs.ps</filename> (CVS 版本可能有所不同)。还可以访问 CVS 主页，位置是 <ulink url="http://www.cvshome.org/">http://www.cvshome.org/</ulink>。</para>
      <tip>
        <title>技巧</title>
        <para>由于 <application>CVS</application> 使用 <application>ssh</application> 来连接 <application>CVS</application> 服务器，在发出 <application>CVS</application> 请求前，您将被提示输入密码。如果您打算配置一下，省去输入密码的步骤，请参考 <ulink url="http://redhat.com/docs/manuals/linux/RHL-9-Manual/custom-guide/s1-openssh-client-config.html"><citetitle>&RH; Linux 9 Customization Guide</citetitle></ulink>(定制指南)中使用 <command>ssh-agent</command> 的细节。</para>
      </tip>
    </section>
  </section>
</chapter>
