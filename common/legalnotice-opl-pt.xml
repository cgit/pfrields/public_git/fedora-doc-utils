<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id: -->
<!DOCTYPE legalnotice PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
 "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [

<!ENTITY % FEDORA-ENTITIES SYSTEM "entities/entities-pt.ent">
%FEDORA-ENTITIES;

]>

<!-- 

     To use the content of this legal notice in another document, simply
     use the XInclude standard.  Logically there are only two elements
     you need to reference, as follows:

     <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
       href="legalnotice-opl-pt.xml"
       xpointer="element(opl.permission)"/>
     <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
       href="legalnotice-opl-pt.xml" xpointer="element(opl.require)"/>

     You may insert these includes in any element which can contain a
     <para> and an <orderedlist> element.

-->


<legalnotice id="legalnotice-opl">
  <para id="opl.permission">
    É cedida a permissão para copiar, distribuir e/ou modificar este documento
    segundo os termos da Open Publication Licence, Versão 1.0 ou qualquer
    versão posterior. Os termos da OPL estão descritos abaixo.
  </para>
  <orderedlist numeration="upperroman" id="opl.terms">
    <listitem id="opl.require">
      <bridgehead>REQUISITOS PARA AS VERSÕES MODIFICADAS E NÃO-MODIFICADAS
      </bridgehead>
      <para>
	Os trabalhos da Open Publication podem ser reproduzidos e distribuídos
        por inteiro ou parcialmente, em qualquer meio físico ou electrónico,
        desde que os termos desta licença sejam adicionados e que esta licença
        ou uma incorporação da mesma por referência (com todas as opções
        eleitas pelo(s) autor(es) e/ou publicador) seja visível na reprodução.
      </para>

      <para>
	O formato correcto para uma incorporação por referência é o seguinte:
      </para>

      <para>
	Copyright (c) &lt;ano&gt; de &lt;nome do autor ou designado&gt;.
	Este material poderá ser distribuído apenas sujeito aos termos
        e condições definidos em diante na Open Publication License,
        vX.Y ou posterior (a última versão está disponível de momento em
	<ulink url="http://www.opencontent.org/openpub/"/>).
      </para>

      <para>
        A referência deverá ser seguida imediatamente de quaisquer opções
        eleitas pelo(s) autor(es) e/ou publicador do documento (ver a secção
        VI). A redistribuição comercial de material licenciado pela Open
        Publication é permitida. Toda a publicação em livro (papel) normal
        deverá obrigar à citação do publicador e autor originais. Os nomes
        do publicador e do autor deverão aparecer em todas as superfícies
        exteriores do livo. Em todas as superfícies exteriores do livro,
        o nome do publicador original deverá ser tão grande quanto o título
        do trabalho e citado como possessivo no que respeita ao título.
      </para>
    </listitem>
    <listitem id="opl.copyright">
      <bridgehead>DIREITOS DE CÓPIA</bridgehead>
      <para>
	Os direitos de cópia de cada Open Publication tem a pertença do(s)
        seu(s) autor(es) ou entidades designadas como tal.
      </para>
    </listitem>
    <listitem id="opl.scope">
      <bridgehead>ÂMBITO DA LICENÇA</bridgehead>

      <para>
        Os termos da licença que se seguem aplicam-se a todos os trabalhos
        da Open Publication, a menos que tal em contrário seja 
        explicitamente referido no documento.
      </para>

      <para>
        A mera agregação de trabalhos da Open Publication ou uma porção de
        um trabalho da Open Publication com outros trabalhos ou programas
        no mesmo meio não deverá fazer com que esta licença se aplique a
        esses outros trabalhos. O trabalho agregado deverá conter um aviso
        que especifique a inclusão do material da Open Publication e o aviso
        de direitos de cópia ('copyright') apropriados.
      </para>

      <para>
	SEVERIDADE. Se qualquer parte desta licença for considerada 
        não-aplicável em qualquer jurisdição, as porções restantes da
        licença mantêm-se em efeito.
      </para>

      <para>
	SEM GARANTIA. Os trabalhos da Open Publication são licenciados
        e disponibilizados "tal e qual" sem garantias de qualque tipo,
        expressas ou implícitas, incluindo, mas não se limitando a,
        as garantias implícitas de mercantibilidade e adequação a um
        determinado fim ou uma garantia de não-infracção.
      </para>
    </listitem>
    <listitem id="opl.modified.works">
      <bridgehead>REQUISITOS PARA OS TRABALHOS MODIFICADOS</bridgehead>

      <para>
	Todas as versões modificadas dos documentos cobertos por esta licença,
        incluindo as traduções, antologias, compilações e documentos parciais,
        deverão obedecer aos seguintes requisitos:
      </para>

      <orderedlist>
	<listitem>
	  <para>
	    A versão modificada deverá ser legendada como tal.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    A pessoa que faz as modificações deverá estar identificada e as
            modificações deverão estar datadas.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    O reconhecimento do autor e do publicador originais, se tal for
            aplicável, dever-se-á manter de acordo com as práticas normais
            de citação académica.
	  </para>
	</listitem>
	<listitem>
	  <para>
            A localização do documento original e não-modificador deverá
            estar identificada.
	  </para>
	</listitem>
	<listitem>
	  <para>
            O(s) nome(s) do(s) autor(es) não poderá ser usado para validar
            ou implicar a aprovação do documento resultante sem a permissão
            dos mesmos.
	  </para>
	</listitem>
      </orderedlist>
    </listitem>
    <listitem id="opl.good-practice">
      <bridgehead>RECOMENDAÇÕES DE BOAS PRÁTICAS</bridgehead>

      <para>
        Para além dos requisitos desta licença, pede-se e é fortemente
        recomendado aos distribuidores que:
      </para>

      <orderedlist>
	<listitem>
	  <para>
            Se estiverem a distribuir trabalhos da Open Publication em
            papel ou CD-ROM, forneça uma notificação por e-mail aos autores
            da sua intenção de redistribuição, pelo menos trinta dias antes
            do congelamento do seu manuscrito ou suporte, para dar tempo aos
            autores para fornecerem documentos actualizados. Esta notificação
            deverá descrever as modificações, se existirem, feitas ao 
            documento.
	  </para>
	</listitem>
	<listitem>
	  <para>
            Todas as modificações substanciais (incluindo as remoções) deverão
            estar marcadas, de forma clara, no documento ou descritas de outra
            forma qualquer num anexo ao documento.
	  </para>
	</listitem>
	<listitem>
	  <para>
            Finalmente, embora não seja obrigatório por esta licença, é 
            considerado de bom tom oferecer uma cópia gratuita de qualquer
            expressão impressa ou em CD-ROM de um trabalho licenciado pela
            Open Publication aos seus autores.
	  </para>
	</listitem>
      </orderedlist>

    </listitem>
    <listitem id="opl.options">
      <bridgehead>OPÇÕES DA LICENÇA</bridgehead>

      <para>
        O(s) autor(es) e/ou publicador de um documento licenciado pela
        Open Publication poderá eleger centras opções, adicionando alguma
        linguagem à referência ou à cópia da licença. Estas opções são
        consideradas parte da instância da licença e deverão ser incluídas
        com a licença (ou na sua incorporação por referência) nos trabalhos
        derivados.
      </para>

      <para>
	A. Proibir a distribuição de versões substancialmente modificadas
        sem a permissão explícita dos autores. A "modificação substancial"
        está definida como sendo uma modificação ao conteúdo semântico do
        documento e exclui meras modificações no formato ou correcções
        tipográficas.
      </para>

      <para>
        Para cumprir isto, adicione a frase 'A distribuição de versões
        substancialmente modificadas deste documento é proibida sem a
        permissão explícita do detentor dos direitos de cópia' na referência
        ou cópia da licença.
      </para>

      <para>
	B. A proibição de qualquer publicação deste trabalho ou de trabalhos
        derivados, por inteiro ou em parte, num livro normal (papel) para
        fins comerciais é proibida, a menos que seja obtida uma permissão
        prévia do detentor dos direitos de cópia.
      </para>

      <para>
        Para cumprir isto, adicione a frase 'A distribuição do trabalho ou
        de derivados do trabalho em qualquer formato de livro normal (papel)
        é proibida, a menos que seja obtida permissão prévia do detentor dos
        direitos de cópia.' na referência ou cópia da licença.
      </para>
    </listitem>
  </orderedlist>
</legalnotice>

<!--
Local variables:
mode: xml
fill-column: 72
End:
-->
