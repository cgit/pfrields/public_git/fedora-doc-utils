<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id: -->
<!DOCTYPE legalnotice PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
 "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [

<!ENTITY % FEDORA-ENTITIES SYSTEM "entities/entities-pt_BR.ent">
%FEDORA-ENTITIES;

]>

<!-- 

     To use the content of this legal notice in another document, simply
     use the XInclude standard.  Logically there are only two elements
     you need to reference, as follows:

     <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
       href="legalnotice-opl-pt_BR.xml"
       xpointer="element(opl.permission)"/>
     <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
       href="legalnotice-opl-pt_BR.xml" xpointer="element(opl.require)"/>

     You may insert these includes in any element which can contain a
     <para> and an <orderedlist> element.

-->


<legalnotice id="legalnotice-opl">
  <para id="opl.permission">
    Permission is granted to copy, distribute, and/or modify this
    document under the terms of the Open Publication Licence, Version
    1.0, or any later version. The terms of the OPL are set out below.
  </para>
  <orderedlist numeration="upperroman" id="opl.terms">
    <listitem id="opl.require">
      <bridgehead>REQUIREMENTS ON BOTH UNMODIFIED AND MODIFIED
	VERSIONS</bridgehead>
      <para>
	Open Publication works may be reproduced and distributed in
	whole or in part, in any medium physical or electronic, provided
	that the terms of this license are adhered to, and that this
	license or an incorporation of it by reference (with any options
	elected by the author(s) and/or publisher) is displayed in the
	reproduction.
      </para>

      <para>
	Proper form for an incorporation by reference is as follows:
      </para>

      <para>
	Copyright (c) &lt;year&gt; by &lt;author's name or designee&gt;.
	This material may be distributed only subject to the terms and
	conditions set forth in the Open Publication License, vX.Y or
	later (the latest version is presently available at <ulink
	  url="http://www.opencontent.org/openpub/"/>).
      </para>

      <para>
	The reference must be immediately followed with any options
	elected by the author(s) and/or publisher of the document (see
	section VI). Commercial redistribution of Open
	Publication-licensed material is permitted. Any publication in
	standard (paper) book form shall require the citation of the
	original publisher and author. The publisher and author's names
	shall appear on all outer surfaces of the book. On all outer
	surfaces of the book the original publisher's name shall be as
	large as the title of the work and cited as possessive with
	respect to the title.
      </para>
    </listitem>
    <listitem id="opl.copyright">
      <bridgehead>COPYRIGHT</bridgehead>
      <para>
	The copyright to each Open Publication is owned by its author(s)
	or designee.
      </para>
    </listitem>
    <listitem id="opl.scope">
      <bridgehead>SCOPE OF LICENSE</bridgehead>

      <para>
	The following license terms apply to all Open Publication works,
	unless otherwise explicitly stated in the document.
      </para>

      <para>
	Mere aggregation of Open Publication works or a portion of an
	Open Publication work with other works or programs on the same
	media shall not cause this license to apply to those other
	works. The aggregate work shall contain a notice specifying the
	inclusion of the Open Publication material and appropriate
	copyright notice.
      </para>

      <para>
	SEVERABILITY. If any part of this license is found to be
	unenforceable in any jurisdiction, the remaining portions of the
	license remain in force.
      </para>

      <para>
	NO WARRANTY. Open Publication works are licensed and provided
	"as is" without warranty of any kind, express or implied,
	including, but not limited to, the implied warranties of
	merchantability and fitness for a particular purpose or a
	warranty of non-infringement.
      </para>
    </listitem>
    <listitem id="opl.modified.works">
      <bridgehead>REQUIREMENTS ON MODIFIED WORKS</bridgehead>

      <para>
	All modified versions of documents covered by this license,
	including translations, anthologies, compilations and partial
	documents, must meet the following requirements:
      </para>

      <orderedlist>
	<listitem>
	  <para>
	    The modified version must be labeled as such.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    The person making the modifications must be identified and
	    the modifications dated.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    Acknowledgement of the original author and publisher if
	    applicable must be retained according to normal academic
	    citation practices.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    The location of the original unmodified document must be
	    identified.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    The original author's (or authors') name(s) may not be used
	    to assert or imply endorsement of the resulting document
	    without the original author's (or authors') permission.
	  </para>
	</listitem>
      </orderedlist>
    </listitem>
    <listitem id="opl.good-practice">
      <bridgehead>GOOD-PRACTICE RECOMMENDATIONS</bridgehead>

      <para>
	In addition to the requirements of this license, it is requested
	from and strongly recommended of redistributors that:
      </para>

      <orderedlist>
	<listitem>
	  <para>
	    If you are distributing Open Publication works on hardcopy
	    or CD-ROM, you provide email notification to the authors of
	    your intent to redistribute at least thirty days before your
	    manuscript or media freeze, to give the authors time to
	    provide updated documents. This notification should describe
	    modifications, if any, made to the document.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    All substantive modifications (including deletions) be
	    either clearly marked up in the document or else described
	    in an attachment to the document.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    Finally, while it is not mandatory under this license, it is
	    considered good form to offer a free copy of any hardcopy
	    and CD-ROM expression of an Open Publication-licensed work
	    to its author(s).
	  </para>
	</listitem>
      </orderedlist>

    </listitem>
    <listitem id="opl.options">
      <bridgehead>LICENSE OPTIONS</bridgehead>

      <para>
	The author(s) and/or publisher of an Open Publication-licensed
	document may elect certain options by appending language to the
	reference to or copy of the license. These options are
	considered part of the license instance and must be included
	with the license (or its incorporation by reference) in derived
	works.
      </para>

      <para>
	A. To prohibit distribution of substantively modified versions
	without the explicit permission of the author(s). "Substantive
	modification" is defined as a change to the semantic content of
	the document, and excludes mere changes in format or
	typographical corrections.
      </para>

      <para>
	To accomplish this, add the phrase 'Distribution of
	substantively modified versions of this document is prohibited
	without the explicit permission of the copyright holder.' to the
	license reference or copy.
      </para>

      <para>
	B. To prohibit any publication of this work or derivative works
	in whole or in part in standard (paper) book form for commercial
	purposes is prohibited unless prior permission is obtained from
	the copyright holder.
      </para>

      <para>
	To accomplish this, add the phrase 'Distribution of the work or
	derivative of the work in any standard (paper) book form is
	prohibited unless prior permission is obtained from the
	copyright holder.' to the license reference or copy.
      </para>
    </listitem>
  </orderedlist>
</legalnotice>

<!--
Local variables:
mode: xml
fill-column: 72
End:
-->
