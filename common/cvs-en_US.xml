<!-- $Id: -->
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
 "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [

<!ENTITY % FDP-ENTITIES SYSTEM "entities/entities-en_US.ent">
%FDP-ENTITIES;

]>


<chapter id="ch-cvs">
  <title>CVS</title>
  <para>
    The Concurrent Versions System (<application>CVS</application>)
    provides a framework for collaborative revision.  Without such a
    framework, a group of users editing files in a single directory
    would cause chaos. Using <application>CVS</application>, however, a
    group of people can safely work on the same set of files. The
    <application>CVS</application> server keeps the master copy of the
    files, and it records all changes and associated data, such as
    authors and time, in a central repository. If conflicts arise,
    <application>CVS</application> advises the users. Programmers often
    use <application>CVS</application> to share code, but it also works
    well for documentation.
  </para>
  <indexterm>
    <primary>cvs</primary>
  </indexterm>
  <section id="sn-cvs-overview">
    <title>How CVS Works</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>how it works</secondary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary>overview</secondary>
    </indexterm>
    <para>
      In most cases, each set of files that make up a package or project
      is stored as a <firstterm>module</firstterm> on the CVS server.
      When working with files from <application>CVS</application>, you
      <firstterm>checkout</firstterm> a copy of the module on your local
      file system. After modifying one or more files, you
      <firstterm>commit</firstterm> them back to the central
      <application>CVS</application> repository server.
    </para>

    <para>
      With <application>CVS</application> you may edit a file without
      first getting permission or locking the file. As long as none of
      the changes overlap, <application>CVS</application> can correctly
      record their changes.  When duplicate changes occur, they are
      clearly marked in the files and the authors must resolve the issue
      among themselves.
    </para>

    <para>
      When you commit changes, only changes to files the server knows
      about are committed. In other words, if you created a file in your
      local checkout of a module, the new file is not automatically
      uploaded to the server. You must <firstterm>add</firstterm> the
      file to the repository and then commit it. If you remove a file
      from your local checkout of a module, you must specify that you
      want to remove it from the repository on the CVS server and then
      commit the removal of the file. The specific commands to perform
      these actions are discussed in <xref
	linkend="sn-cvs-cvscommands"/>.
    </para>

    <para>
      If someone has modified the file between the last time you grabbed
      the file from CVS and when you try to commit a change,
      <application>CVS</application> will try to merge the changes into
      the master copy of the <application>CVS</application> server. If
      the content you changed is in a different location in the file
      than the content changed by someone else, the commit action will
      likely go through without a <firstterm>conflict</firstterm>. If
      someone modified the same content as the content you just changed
      and tried to commit, you will see a message that a file conflict
      has occurred. Thus, you need to <firstterm>update</firstterm> your
      files frequently. It is a good practice to update them right
      before you start modifying a file. Refer to <xref
	linkend="sn-cvs-cvscommands-conflicts"/> for instructions on
      resolving conflicts.
    </para>
  </section>
  <section id="sn-cvs-preparation">
    <title>Preparing For CVS Use</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>preparing for use</secondary>
    </indexterm>
    <para>
      Before using <application>CVS</application>, you need to establish
      an account with the <application>CVS</application> server. After
      you get an account, you do not need to perform these actions
      again.
    </para>

    <section id="sn-cvs-rpm-check">
      <title>Is CVS Installed On Your System</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>RPM installation</secondary>
      </indexterm>
      <para>
        You must have the <application>CVS</application>
        <abbrev>RPM</abbrev> package installed. Verify its presence by
        typing the command:
      </para>
      <screen><command>rpm -q cvs</command></screen>
      <para>
        If you see output similar to
        <computeroutput>cvs-1.11.19-1</computeroutput>, then the package
        is installed. A message similar to <computeroutput>package cvs
        is not installed</computeroutput> means you must install the
        <application>cvs</application> package before continuing. If you
        do not know how to do this, consult your system administrator
        who can install it for you.
      </para>
    </section>

    <section id="sn-cvs-generate-keys">
      <title>Generating SSH Keys</title>
      <indexterm>
        <primary>OpenSSH</primary>
        <secondary>authorization keys</secondary>
      </indexterm>
      <para>
        The <application>CVS</application> server uses
        <application>SSH</application> Protocol 2 keys to authenticate
        users. Thus, you need to generate a pair of keys before applying
        for a <application>CVS</application> account. If you already
        have an <application>SSH</application> <abbrev>DSA</abbrev> key,
        you may skip this step.
      </para>
      <tip>
        <title>Tip</title>
        <para>
          You already have a <abbrev>DSA</abbrev> key if you have the
          file <filename>~/.ssh/id_dsa.pub</filename> on the system.
        </para>
        <para>
          If your existing <abbrev>DSA</abbrev> key does not require a
          <wordasword>passphrase</wordasword>, you are strongly urged to
          generate one that does require a passphrase.
        </para>
      </tip>
      <para>
        Use the following steps to generate a <abbrev>DSA</abbrev> key
        used by <application>SSH</application> Protocol 2. It is
        required for an
        <computeroutput>cvs.fedoraproject.org</computeroutput>
        <application>CVS</application> account.
      </para>
      <orderedlist>
        <indexterm>
          <primary>OpenSSH</primary>
          <secondary><command>ssh-keygen</command>
          </secondary>
        </indexterm>
        <indexterm>
          <primary><command>ssh-keygen</command>
          </primary>
        </indexterm>
        <listitem>
          <para>
            To generate a
            <acronym>DSA</acronym>
            key to work with version 2.0 protocol, at a shell prompt,
            type the command:
          </para>
	  <screen><command>ssh-keygen -t dsa</command></screen>
          <para>
            Accept the default file location of
            <filename>~/.ssh/id_dsa</filename>. You are strongly urged
            to define and use a <firstterm>passphrase</firstterm> to
            enhance the security of your key. Enter a passphrase
            different than your account password and confirm it by
            entering it again.
          </para>
        </listitem>
        <listitem>
          <para>
            Copy your new key to the correct file by typing the
            following at a shell prompt.
          </para>
	  <screen><command><![CDATA[cat ~/.ssh/id_dsa.pub >> ~/.ssh/authorized_keys]]></command></screen>
          <note>
            <para>
              Check this command carefully before you press the
	      <keycap>Enter</keycap> key. If
	      <filename>~/.ssh/authorized_keys</filename> already
	      exists, the contents of
	      <filename>~/.ssh/id_dsa.pub</filename> will be appended to
	      the end of the <filename>~/.ssh/authorized_keys</filename>
	      file.
            </para>
          </note>
        </listitem>
        <listitem>
          <para>
            Change the permissions of your <filename>~/.ssh</filename>
            directory and your keys with the commands:
          </para>
	  <screen><command>chmod 700 ~/.ssh</command>
<command>chmod 644 ~/.ssh/authorized_keys</command></screen>
        </listitem>
      </orderedlist>
      <tip>
        <title>Tip</title>
        <para>
          You can have your system remember your passphrase so that you
          do not have to type it every time you access the
          <application>CVS</application> server. Refer to the
          documentation of the <application>ssh-add</application>
          program.
        </para>
      </tip>
    </section>
  </section>
  <section id="sn-cvs-config">
    <title>Configuring For CVS Access</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>configuring for access</secondary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary>CVSROOT</secondary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary>CVS_RSH</secondary>
    </indexterm>
    <indexterm>
      <primary>CVSROOT</primary>
    </indexterm>
    <indexterm>
      <primary>CVS_RSH</primary>
    </indexterm>
    <indexterm>
      <primary>cvs</primary>
      <secondary><filename>.cvsrc</filename>
      </secondary>
    </indexterm>
    <indexterm>
      <primary><filename>.cvsrc</filename>
      </primary>
    </indexterm>
    <section id="sn-cvs-config-cvsrc">
      <title>Avoiding Repetitive Typing</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>avoiding repetitive typing</secondary>
      </indexterm>
      <para>
        Many <application>CVS</application> commands need certain
        command line switches to operate consistently. Rather than
        typing them every time that command is used, you can save the
        switches in a file that <application>CVS</application> will read
        before executing your command line.
      </para>

      <para>
        Create a file named <filename>~/.cvsrc</filename> in your home
        directory. It should contain the following commands, one per
        line:
	<screen><computeroutput><![CDATA[cvs -z3
diff -uNp
rdiff -uNp
update -dP]]></computeroutput></screen>
      </para>
    </section>

    <section id="sn-cvs-config-anon">
      <title>Configuring for Read-Only CVS Access</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>configuring read-only access</secondary>
      </indexterm>
      <indexterm>
        <primary>cvs</primary>
        <secondary>anonymous access</secondary>
      </indexterm>
      <para>
        If your goal is to download the various &FC; documents and to
        render them on your system, you only need read-only access to
        the <application>CVS</application> repository. Follow the
        instructions in this section and then skip directly to
        <xref linkend="sn-cvs-cvscommands-co"/>.
      </para>

      <para>
        Change directories to where you want your files from
        <application>CVS</application> to be located, and execute the
        following commands:
      </para>
      <screen><command>export CVSROOT=:pserver:anonymous@cvs.fedoraproject.org:/cvs/docs</command>
<command>cvs login</command>
<command>cvs checkout</command> <replaceable>module-name</replaceable>
<command>cvs checkout</command> <replaceable>module-name</replaceable>
<command>cd</command> <replaceable>module-name</replaceable></screen>
      <para>
        Once you have checked the module out, the value of your
	<envar>$CVSROOT</envar> environment variable does not matter.
	It is stored in the file <filename>CVS/Root</filename>
	for each directory in your local repository. As long as your
	current working directory has a <filename>CVS/</filename>
	directory, the <application>CVS</application> program will
	automatically locate the &FDP; repository.
      </para>
    </section>

    <section id="sn-cvs-config-author">
      <title>Configuring Read/Write CVS Access</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>configuring read/write access</secondary>
      </indexterm>
      <indexterm>
        <primary>cvs</primary>
        <secondary>configuring access for authors</secondary>
      </indexterm>
      <para>
        To author a new document or to change an existing one, you must
        obtain full read/write access to the &FDP;
        <application>CVS</application> repository. For the full details
        on this process, refer to the
        <ulink url="http://fedoraproject.org/wiki/DocsProject/NewWriters"><filename>http://fedoraproject.org/wiki/DocsProject/Join</filename>
        </ulink> web site.
      </para>

      <para>
        Every author has a unique <envar>$CVSROOT</envar> to access the
	<application>CVS</application> repository:
      </para>
      <screen><command>export CVSROOT=:ext:<replaceable>yourname</replaceable>@cvs.fedoraproject.org:/cvs/docs</command>
<command>export CVS_RSH=/usr/bin/ssh</command></screen>
      <para>
        With the <envar>$CVSROOT</envar> and <envar>$CVS_RSH</envar>
	environment variables in place, you can access the repository:
      </para>
      <screen><command>cvs co -c</command></screen>
      <para>
        You will be asked for the passphrase for your
	<application>SSH</application> key. Press <keycap>Enter</keycap>
	to receive a list of modules already in the repository.
      </para>
    </section>
  </section>
  <section id="sn-cvs-cvscommands">
    <title>Basic CVS Commands</title>
    <indexterm>
      <primary>cvs</primary>
      <secondary>commands</secondary>
    </indexterm>
    <para>
      After configuring your system to work with CVS, checkout the
      modules you will be working on.
    </para>
    <tip>
      <title>Tip</title>
      <para>
        To see if you need a correctly-set <envar>$CVSROOT</envar>
	variable, or the <option>-m </option>
	<replaceable>repository</replaceable> command line switch, see
	if you have a <filename>CVS/</filename> subdirectory in your
	working directory.
      </para>
      <para>
        If you have a <filename>CVS/</filename> directory,
	<application>CVS</application> ignores any
	<envar>$CVSROOT</envar> or <option>-m</option> command line
	switch.
      </para>
    </tip>
    <section id="sn-cvs-cvscommands-co">
      <title>Checking Out Modules</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>checking out modules</secondary>
      </indexterm>
      <para>
        You only need to checkout a module once. Once a local copy of
	the module is on your system, you may continue to use it for
	additional work.
      </para>

      <para>
        To checkout a module, use the following command:
      </para>
      <screen><command>cvs co <replaceable>module-name</replaceable></command></screen>
      <para>
        For example, to checkout the
        <computeroutput>example-tutorial</computeroutput> module, change
        to your work directory, and execute the following command:
      </para>
      <screen><command>cvs co example-tutorial</command></screen>
      <para>
        A directory called <filename>example-tutorial/</filename> is
        created in the current directory.
      </para>

      <para>
        If a branch name is not specified when checking out a module, it
        is referred to as the <firstterm>HEAD</firstterm> of the
        <application>CVS</application> module.
      </para>

      <section id="sn-cvs-cvscommands-co-branch">
        <title>Checking Out Branches of Modules</title>
        <indexterm>
          <primary>cvs</primary>
          <secondary>check out modules</secondary>
          <tertiary>checking out branches</tertiary>
        </indexterm>
        <para>
          Think of a <application>CVS</application> branch as a version
          of the files for a particular version of a manual or package.
        </para>

        <para>
          To checkout a branch of a module, use the following command:
        </para>
	<screen><command>cvs co -d <replaceable>directory</replaceable> -r <replaceable>branchname</replaceable> <replaceable>module-name</replaceable></command></screen>
        <para>
          A directory named <replaceable>directory</replaceable>
          is created, and the files for the
          <replaceable>branchname</replaceable> branch of the
          <replaceable>module-name</replaceable> module are
          copied in the directory.
        </para>

        <para>
          For example, to checkout a branch named
	  <systemitem>BRANCH-VERSION-1.2</systemitem> from the
	  <computeroutput>mymodule</computeroutput> module, use the
	  command:
        </para>
	<screen><command>cvs co -d mymodule-1.2 -r BRANCH-VERSION-1.2 mymodule</command></screen>
        <para>
          The BRANCH-VERSION-1.2 branch of the module is checked out in
          the <filename>mymodule-1.2</filename> directory on your
          system.
        </para>

        <para>
          To determine which branches and tags exist for a file, use the
          command:
        </para>
	<screen><command>cvs status -v <replaceable>filename</replaceable></command></screen>
        <para>
          For example, the status of the file
          <filename>foo.xml</filename> is as follows:
        </para>
<screen>
<computeroutput><![CDATA[===================================================================
File: foo.xml     Status: Up-to-date

   Working revision:    1.47
   Repository revision: 1.47    /cvs/docs/foo-guide/foo.xml,v
   Sticky Tag:          (none)
   Sticky Date:         (none)
   Sticky Options:      (none)

   Existing Tags:
        BRANCH-VERSION-1.2              (branch: 1.25.2)]]></computeroutput></screen>
        <para>
          Only tags marked as branches in the second column under the
          <computeroutput>Existing Tags</computeroutput> section can be
          checked out as a branch.
        </para>
      </section>
    </section>

    <section id="sn-cvs-cvscommands-up">
      <title>Updating Files</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>updating files</secondary>
      </indexterm>
      <para>
        To retrieve the latest versions of the files in a module, change
        to the directory that contains the files for the module and
        execute the command:
      </para>
      <screen><command>cvs update</command></screen>
      <para>
        The latest versions of all the files in the module are
        downloaded into your local copy. If you notice a file conflict,
        refer to <xref linkend="sn-cvs-cvscommands-conflicts"></xref>.
      </para>
    </section>

    <section id="sn-cvs-cvscommands-commit">
      <title>Committing Files</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>committing files</secondary>
      </indexterm>
      <para>
        After modifying files in your local version of a module, commit
        them to save the changes on the <application>CVS</application>
        server:
      </para>
      <screen><command>cvs commit -m "<replaceable>some log message</replaceable>" <replaceable>filename</replaceable></command></screen>
      <note>
        <para>
          If you would prefer to write your log message with your
	  favorite text editor, as defined by the <envar>$VISUAL</envar>
	  or the <envar>$EDITOR</envar> environment variable, just omit
	  the <userinput>-m "<replaceable>some log
	      message</replaceable>"</userinput>. The buffer will
	  already contain comments describing the change.  You do not
	  need to delete them as you enter your own text.
        </para>
      </note>

      <para>
        The log message should be as descriptive as possible so that you
        and anyone else working on the module understands what changed.
        Using a log message such as <userinput>updated some
        files</userinput> does not accurately describe what has changed
        and will not help you in the future. If you are correcting a
        bug, use the <application>Bugzilla</application> reference.
      </para>

      <para>
        The <replaceable>filename</replaceable> can be one
        filename, a series of filenames separated by spaces, or a group
        of filenames specified using wildcards such as
        <filename>*.png</filename> or <filename>foo-*.xml</filename>.
      </para>

      <para>
        If no filename or group of filenames is specified in the
        <command>commit</command> command, all outstanding changes of
        any kind are committed to the server. The command is recursive
        and will include changes in any subdirectories of the
        module.</para>
      <caution>
	<title>Committing Changes</title>
	<para>
	  Use caution when issuing the <command>commit</command> command
	  without any filenames because you might not remember exactly
	  what files changed.
	</para>
      </caution>
      <para>
        If you notice a file conflict, refer to
        <xref linkend="sn-cvs-cvscommands-conflicts"></xref>.
      </para>
    </section>

    <section id="sn-cvs-cvscommands-add">
      <title>Adding Files</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>adding files</secondary>
      </indexterm>
      <para>
        To add a file to a module, create the file in your local copy
        then execute the following command:
      </para>
      <screen><command>cvs add</command> <replaceable>file-to-add</replaceable></screen>
      <para>
        After adding the file, you must <command>commit</command> the
        <command>add</command> to copy it to the server:
      </para>
      <screen><command>cvs commit -m "<replaceable>some log message</replaceable>" <replaceable>file-to-add</replaceable></command></screen>
    </section>

    <section id="sn-cvs-cvscommands-admin">
      <title>Managing Binary Files</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>binary files</secondary>
      </indexterm>
      <para>
        The most commonly-archived files are simple text files, but
	sometimes binary files are also archived. The
	<application>cvs</application> program recognizes most common
	filename extensions such as <filename>.png</filename> or
	<filename>.jpg</filename>, and usually behaves as the user
	intends.
      </para>

      <para>
        When a copy of a file is checked out of the repository,
	<application>cvs</application> scans it for special keywords
	such as <computeroutput>$id:$</computeroutput>. The
	<application>cvs</application> program replaces the keyword with
	a generated value such as the file version number.
      </para>

      <para>
        This keyword substitution usually corrupts binary files, so it
        must be turned off if <application>cvs</application> does not
        recognize your file as binary. To mark your file as being
        binary, and thus needing the keyword expansion turned off, use
        the command:
      </para>
      <screen><command>cvs admin -kk <replaceable>filename</replaceable></command></screen>
      <para>
        Note that the file must already be checked in to the
	<abbrev>CVS</abbrev> repository before the
	<option>admin</option> command can be used. This is acceptable,
	since the keyword expansion is done as the file is checked out
	and copied to the local directory, not when the file is
	committed to the repository.
      </para>
      <tip>
        <title>Recovering a binary file</title>
        <para>
          If you check a binary file into the repository and then find
	  it corrupted when it is checked out, do not panic. Use the
	  <option>admin</option> command as described above, delete your
	  local file copy, and check it out again.
        </para>
      </tip>
    </section>

    <section id="sn-cvs-cvscommands-rm">
      <title>Removing Files</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>removing files</secondary>
      </indexterm>
      <para>
        If a file is no longer needed in the module, use the
        <command>remove</command> command to remove it from your local
        copy and then <command>commit</command> the removal to the
        server. Even though the file is removed from current version of
        the module, an archived copy is still kept on the server and can
        be retrieved at any time with the <command>add</command>
        command.
      </para>
      <screen><command>cvs rm -f</command> <replaceable>file-to-remove</replaceable></screen>
      <para>
        After removing the file, you must <command>commit</command> the
        removal:
      </para>
      <screen><command>cvs commit -m "<replaceable>some log message</replaceable>" <replaceable>file-to-remove</replaceable></command></screen>
      <para>
        You can not use wildcards in the <command>commit</command>
        command to identify removed files. They must be specified with a
        exact filename.
      </para>

      <para>
        If you need to rename a file, it is best to rename the file on
        the <application>CVS</application> server so that the history of
        the file is preserved. If you need to rename a file, send an
        email to
        <ulink url="mailto:cvsdocs-administrator@fedoraproject.org">cvsdocs-administrator@fedoraproject.org</ulink> asking to have the file renamed.
      </para>
    </section>

    <section id="sn-cvs-cvscommands-status">
      <title>Status of Files</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>status of files</secondary>
      </indexterm>
      <para>
        Sometimes it is necessary to view the
        <firstterm>status</firstterm> of a file in a
        <application>CVS</application> module. To view the status of a
        file, use the command:
      </para>
      <screen><command>cvs status</command> <replaceable>filename</replaceable></screen>
      <para>
        The status report of a repository file is as follows:
      </para>

      <variablelist>
        <varlistentry>
          <term><computeroutput>Up-to-date</computeroutput></term>
          <listitem>
            <para>
              Your revision of the file is identical to the latest
              revision on the <application>CVS</application> server.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>Locally Modified</computeroutput></term>
          <listitem>
            <para>
              You have updated to the latest revision from the server,
              but then you modified the file on your system.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>Locally Added</computeroutput></term>
          <listitem>
            <para>
              You added the file with the <command>cvs add</command>
              command but have not yet committed the addition of the
              file.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>Locally Removed</computeroutput></term>
          <listitem>
            <para>
              You removed the file with the <command>cvs
              remove</command> command but have not yet committed the
              removal.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>Needs Checkout</computeroutput></term>
          <listitem>
            <para>
              A newer version of the file is on the server and needs to
              be retrieved. Even though the status includes the word
              checkout, it really means that you need to update your
              files with the <command>cvs update</command> command.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>Needs Patch</computeroutput></term>
          <listitem>
            <para>
              The revision in your local checkout needs a patch to be
              the latest revision from the server. Issue the
              <command>cvs update</command> command to resolve.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>Needs Merge</computeroutput></term>
          <listitem>
            <para>
              A newer revision exists on the server and your local
              version contains modification not yet committed. This
              status usually occurs if you don't have the latest
              revision of the file and edit it anyway.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>File had conflicts on merge</computeroutput></term>
          <listitem>
            <para>
              Similar to <computeroutput>Needs Merge</computeroutput>,
              except when you tried to issue the <command>cvs
              update</command> command, the differences could not be
              resolved automatically. Refer to
              <xref linkend="sn-cvs-cvscommands-conflicts"></xref> for
              more information on resolving conflicts.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term><computeroutput>Unknown</computeroutput></term>
          <listitem>
            <para>
              The <application>CVS</application> server does not know
              anything about this file. It has neither been added nor
              removed locally and has never been committed to the
              server. This status usually occurs for files you should
              not commit to <application>CVS</application> such as
              <filename>generated-index.sgml</filename> or for files
              that you want to add to the repository but have not yet
              issued the <command>cvs add</command> command.
            </para>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>

    <section id="sn-cvs-cvscommands-conflicts">
      <title>Resolving Conflicts</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>resolving conflicts</secondary>
      </indexterm>
      <para>
        If you modify a file and the same region is modified by someone
        else and committed first, you will probably see a message
        similar to the following when committing the file or updating
        your local copy of the module:
      </para>
      <screen><computeroutput><![CDATA[RCS file: /cvs/docs/module-name/filename.sgml,v
retrieving revision 1.12
retrieving revision 1.13
Merging differences between 1.12 and 1.13 into filename.sgml
rcsmerge: warning: conflicts during merge
cvs server: conflicts found in filename.sgml
C filename.sgml]]></computeroutput></screen>
      <para>
        To resolve the conflict, open the file, search for
        <computeroutput><![CDATA[<<<<<<<]]></computeroutput>
        and determine which version of the content is correct. For
        example:
      </para>
      <screen><computeroutput><![CDATA[<para>
  Some sentence.
<<<<<<< filename.sgml
  A sentence that was changed in the working copy.
=======
  A same sentence that was changed differently and committed.
>>>>>>> 1.13
</para>]]></computeroutput></screen>
      <para>
        The content between the
        <computeroutput><![CDATA[<<<<<<<]]></computeroutput>,
        and the <computeroutput>=======</computeroutput> is the content
        from your working copy. The content between the
        <computeroutput>=======</computeroutput> and the
        <computeroutput><![CDATA[>>>>>>>]]></computeroutput> is
        the content from the server.
      </para>

      <para>
        Resolve the conflict by editing your copy, and commit the file.
      </para>
    </section>
    <!-- Stopped editing here temporarily :: PWF 2007-06-25 -->
    <section id="sn-cvs-cvscommands-summary">
      <title>Summary</title>
      <indexterm>
        <primary>cvs</primary>
        <secondary>commands</secondary>
        <tertiary>summary of</tertiary>
      </indexterm>
      <para>
        All commands assume you are in the proper directory for the
        <application>CVS</application> module.
      </para>
      <table frame="all" id="tb-cvs-basic-commands">
        <title>Basic CVS Commands</title>
        <tgroup cols="2">
          <colspec colnum="1" colname="shortcut" colwidth="30"/>
          <colspec colnum="2" colname="description" colwidth="60"/>
          <thead>
            <row>
              <entry>Command</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><command>cvs checkout
        <replaceable>&lt;module-name&gt;</replaceable></command>
      or <command>cvs co <replaceable>&lt;module-name&gt;</replaceable></command>
              </entry>
              <entry>Creates a directory called
      <replaceable>&lt;module-name&gt;</replaceable> with the contents of the
      module in the directory</entry>
            </row>
            <row>
              <entry><command>cvs co -d <replaceable>&lt;directory&gt;</replaceable> -r <replaceable>&lt;branchname&gt;</replaceable><replaceable>&lt;module-name&gt;</replaceable></command>
              </entry>
              <entry>Creates the <replaceable>&lt;directory&gt;</replaceable> directory
      with the contents of the <replaceable>&lt;branchname&gt;</replaceable>
      branch of the <replaceable>&lt;module-name&gt;</replaceable> module</entry>
            </row>
            <row>
              <entry><command>cvs update</command> or <command>cvs up</command>
              </entry>
              <entry>Update your files with the latest files from the CVS server</entry>
            </row>
            <row>
              <entry><command>cvs add <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>Add a new file "filename" to the CVS server</entry>
            </row>
            <row>
              <entry><command>cvs commit -m "My message"
        <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>Update file <replaceable>&lt;filename&gt;</replaceable> with the
      latest copy from your computer</entry>
            </row>
            <row>
              <entry><command>cvs log <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>View the commit messages for the file <replaceable>&lt;filename&gt;</replaceable>
              </entry>
            </row>
            <row>
              <entry><command>cvs status <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>View status of the file, such as <computeroutput>Locally
        Modified</computeroutput>
              </entry>
            </row>
            <row>
              <entry><command>cvs status -v <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>View existing tags and branches for file</entry>
            </row>
            <row>
              <entry><command>cvs diff <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>Show diff of the working copy of the file and the latest
      version of the file for the branch</entry>
            </row>
            <row>
              <entry><command>cvs diff -r1.1 -r1.2 <replaceable>&lt;filename&gt;</replaceable></command>
              </entry>
              <entry>Show diff of version 1.1 and 1.2 for file</entry>
            </row>
          </tbody>
        </tgroup>
      </table>
      <para>
        For more information, read the CVS manual available on your
        system at
        <filename>/usr/share/doc/cvs-<replaceable>&lt;version-number&gt;</replaceable>/cvs.ps</filename>
        (the CVS version might vary) and visit the CVS webpage available
        at
        <ulink url="http://www.cvshome.org/">http://www.cvshome.org/</ulink>.
      </para>
      <tip>
        <title>Tip</title>
        <para>
          Since <application>CVS</application> is using
          <application>ssh</application> to connect to the
          <application>CVS</application> server, you will be prompted
          your password before performing your
          <application>CVS</application> request. If you want to
          configure your machine so that you do not have to enter a
          password, refer to the <ulink
          url="http://www.redhat.com/docs/manuals/enterprise/RHEL-5-manual/en-US/RHEL510/Deployment_Guide/ch-openssh.html"><citetitle>&RHEL;
          Deployment Guide</citetitle></ulink> for details about using
          <command>ssh-agent</command>.
        </para>
      </tip>
    </section>
  </section>
</chapter>
