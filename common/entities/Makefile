PRI_LANG=en_US
OTHERS =bal bn_IN ca cs da de el en es fi fr gu he hi_IN hr hu id it ja ml ms nb nl pa pl pt_BR pt ru sk sr sr_Latn sv ta uk zh_CN zh_TW

#######################################################################
# PLEASE:
# TRANSLATORS SHOULD NOT MAKE ANY CHANGES BELOW THIS LINE.
# THANK YOU.
#######################################################################

#######################################################################
# Populate make(1) with only those suffixes of interest to us
#
.SUFFIXES:
.SUFFIXES:	.ent	.po	.pot	.xml	.xsl
#
#######################################################################

#######################################################################
# Define macros to explicitly instantiate the files to work with
#
OTHERXMLFILES=$(foreach L,${OTHERS},entities-${L}.xml)
POFILES=$(foreach L,${OTHERS},po/${L}.po)
ENTFILES=$(foreach L,${PRI_LANG} ${OTHERS},entities-${L}.ent)
#
#######################################################################

#######################################################################
# Printf(1) format string for describing targets by "make help".
HELPFMT	="%-23s\t| %s.\n"
#
#######################################################################

#######################################################################
FDPCOMMONDIR:=$(shell cd .. && /bin/pwd)
#######################################################################

#######################################################################
# WARNING: put no targets before this one!
#
.PHONY:	all
all::	${ENTFILES}
#######################################################################

#######################################################################
# Target 'help' displays the available targets
#
.PHONY:	help
help::
	@printf ${HELPFMT} "make all" 	"Generate .ENT files"
	@printf ${HELPFMT} "make help" 	"Display this list"
#
#######################################################################

#######################################################################
# Target 'entities.pot' makes a seed translation file.  We
# use the original-language entities XML file as the source.
#
.PHONY:	pot

pot po/entities.pot:: entities-${PRI_LANG}.xml
	xml2po -o po/entities.pot $<

distclean::
	${RM} po/entities.pot

help::
	@printf ${HELPFMT} "make pot" 		"Create template PO file"
	@printf ${HELPFMT} "make po/entities.pot" "Create template PO file"
#
#######################################################################

#######################################################################
# 'PO_template' emits rules to update the locale-specific .PO file
# whenever the "entities-${PRI_LANG}.xml", and therefore the 
# "entities.pot" file, is changed.
#
define	PO_template
.PHONY:	po-${1}
.PRECIOUS: po/${1}.po
po-${1}:: po/${1}.po
po/${1}.po:: po/entities.pot
	if [ ! -f po/${1}.po ]; then					\
		cp $$< po/${1}.po;					\
	else								\
		msgmerge --update --backup=simple po/${1}.po $$<;	\
	fi

postat-${1}::
	@echo "${1}: $(shell msgfmt --stat po/${1}.po 2>&1 | \
                sed 's@[^0-9]\+@/@g')"

help::
	@printf ${HELPFMT} "make po-${1}"	"Generate .PO file for locale '${1}'"
	@printf ${HELPFMT} "make po/${1}.po"	"Generate '${1}.po' file"
endef

$(foreach L,${OTHERS},$(eval $(call PO_template,${L})))

.PHONY:	po-all po
po-all po:	${foreach L,${OTHERS},po/${L}.po}
postat:  	$(foreach L,${OTHERS},postat-${L})

help::
	@printf ${HELPFMT} "make po-all"	"Generate all .PO files"
#
#######################################################################

#######################################################################
# 'UPDATE_template' generates the rules to derive the translated XML
# files based on the "entities-${PRI_LANG}.xml" file and the selected
# "${LANG}.po" file.
#
define UPDATE_template
.PHONY:	xml-${1}
xml-${1}:: entities-${1}.xml
entities-${1}.xml:: entities-${PRI_LANG}.xml po/${1}.po
	xml2po -p po/${1}.po entities-${PRI_LANG}.xml >entities-${1}.xml

help::
	@printf ${HELPFMT} "make xml-${1}"	"Translate XML for locale '${1}'"
	@printf ${HELPFMT} "make entities-${1}.xml" "Build 'entities-${1}.xml' file"
endef

.PHONY:	xml-${PRI_LANG}
xml-${PRI_LANG}:: entities-${PRI_LANG}.xml

$(foreach L,${OTHERS},$(eval $(call UPDATE_template,${L})))

.PHONY:	xml-all
xml-all:: $(foreach L,${OTHERS},entities-${L}.xml)

help::
	@printf ${HELPFMT} "make xml-all"	"Translate XML for all locales"
#
#######################################################################


#######################################################################
# Target 'showvars' displays both provided and computed macro values
#
.PHONY:	showvars
showvars::
	@echo "PRI_LANG=${PRI_LANG}"
	@echo "OTHERS=${OTHERS}"
	@echo "OTHERXMLFILES=${OTHERXMLFILES}"
	@echo "ENTFILES=${ENTFILES}"
	@echo "POFILES=${POFILES}"
	@echo "FDPCOMMONDIR=${FDPCOMMONDIR}"
help::
	@printf ${HELPFMT} "make showvars"	"Show certain macro values"
#
#######################################################################

#######################################################################
# Target 'clean' deletes all temporary files.
#
.PHONY:	clean
clean::
	${RM} *~
	${RM} ${OTHERXMLFILES}

help::
	@printf ${HELPFMT} "make clean"	"Delete all temporary files"
#
#######################################################################

#######################################################################
# Target 'distclean' or 'clobber' deletes all generated product files
# in addition to all temporary files.
#
.PHONY:	distclean clobber
distclean clobber:: clean
	${RM} ${ENTFILES}

help::
	@printf ${HELPFMT} "make distclean" "Delete all generated files"
	@printf ${HELPFMT} "make clobber"   "Delete all generated files"
#
#######################################################################

#######################################################################
define	ENT_template
entities-${1}.ent::	entities-${1}.xml po/entities.pot
	xsltproc -o $$@ --stringparam FDPCOMMONDIR "${FDPCOMMONDIR}" \
		--stringparam LANG "${1}" \
		entities.xsl entities-${1}.xml
endef
$(foreach L,${PRI_LANG} ${OTHERS},$(eval $(call ENT_template,${L})))
#######################################################################
