<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes" omit-xml-declaration="yes"/>

  <xsl:param name="FDPCOMMONDIR">/usr/share/fedora/doc/docs-common/common</xsl:param>
  <xsl:param name="LANG">en_US</xsl:param>

  <xsl:template match="/">
    <xsl:comment>
      <xsl:value-of select="' Group: Automatic Entities '"/>
    </xsl:comment>
    <xsl:choose>
      <xsl:when test="$FDPCOMMONDIR != 'NONE'">
        <xsl:call-template name="do_entity">
          <xsl:with-param name="name">FDPCOMMONDIR</xsl:with-param>
          <xsl:with-param name="desc">Base directory</xsl:with-param>
          <xsl:with-param name="extra" />
          <xsl:with-param name="body" select="$FDPCOMMONDIR"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="$LANG != 'NONE'">
	<xsl:call-template name="do_entity">
	  <xsl:with-param name="name">LANG</xsl:with-param>
	  <xsl:with-param name="desc">Locale code</xsl:with-param>
	  <xsl:with-param name="extra" />
	  <xsl:with-param name="body" select="$LANG"/>
	</xsl:call-template>
      </xsl:when>
    </xsl:choose>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="entities">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="entity">
    <xsl:variable name="name" select="name"/>
    <xsl:variable name="desc">
      <xsl:choose>
        <xsl:when test="comment = ''">
          <xsl:text>FIXME: PLEASE DEFINE ME</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="comment"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="extra">
      <xsl:choose>
        <xsl:when test="@kind = 'system'">
          <xsl:text> SYSTEM</xsl:text>
        </xsl:when>
        <xsl:when test="@kind = 'fdpcommon'">
          <xsl:text> SYSTEM</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text></xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="body">
      <xsl:apply-templates/>
    </xsl:variable>
    <xsl:call-template name="do_entity">
      <xsl:with-param name="name" select="@name"/>
      <xsl:with-param name="desc" select="$desc"/>
      <xsl:with-param name="extra" select="$extra"/>
      <xsl:with-param name="body">
        <xsl:choose>
          <xsl:when test="@kind = 'fdpcommon'">
            <xsl:value-of select="concat( $FDPCOMMONDIR, '/', normalize-space($body))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="normalize-space($body)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="use">
    <xsl:variable name="content" select="@entity"/>
    <xsl:value-of select="normalize-space(concat( '&amp;', $content, &apos;;&apos;))" disable-output-escaping="yes"/>
  </xsl:template>

  <xsl:template match="group">
    <xsl:comment>
      <xsl:value-of select="concat( ' Group: ', @name, ' ')"/>
    </xsl:comment>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="comment"/>

  <xsl:template match="title">
    <xsl:comment>
      <xsl:text> </xsl:text><xsl:copy-of select="."/><xsl:text> </xsl:text>
    </xsl:comment>
  </xsl:template>

  <xsl:template match="ulink">
    <xsl:text disable-output-escaping="yes">&lt;ulink url=&apos;</xsl:text>
    <xsl:value-of select="normalize-space(@url)" disable-output-escaping="yes"/>
    <xsl:text disable-output-escaping="yes">&apos;/&gt;</xsl:text>
  </xsl:template>

  <xsl:template name="do_entity">
    <xsl:param name="name">NAME</xsl:param>
    <xsl:param name="desc">DESC</xsl:param>
    <xsl:param name="extra"></xsl:param>
    <xsl:param name="body">BODY</xsl:param>
    <xsl:value-of select="concat( '&lt;!ENTITY ', $name, ' ', normalize-space($extra), ' &quot;', normalize-space($body), '&quot; &gt; &lt;!-- ', normalize-space($desc), ' --&gt;')" disable-output-escaping="yes"/>
  </xsl:template>

</xsl:stylesheet>
