	How To Add Support For A New Locale

	1) Append the ISO locale code, as either xx or xx_YY, to the
	   ${OTHER} macro definition in the file "Makefile".  Below,
	   we will refer to the new locale as ${LANG}.

	2) Create a .PO file for the locale by typing the command:

	   $ make po/${LANG}.po

	3) Add the translations using your favorite .PO file editor,
	   such as kbabel(1) or gtranslator(1).

	4) Review your updated XML file using the command:

	   $ make xml-${LANG}

	   and reviewing the file "entities-${LANG}.xml" for
	   correctness.

	5) Repeat steps #3 and #4 until a correct XML file is
	   produced.

	6) Commit the PO file.  Do not commit any additional .xml or
           .ent files:

	   $ cvs ci -m 'Added new translation' po/${LANG}.po

Tommy Reynolds
rev. Paul W. Frields
