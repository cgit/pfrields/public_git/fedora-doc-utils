# translation of pl.po to Polish
# Piotr Drąg <raven@pmail.pl>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: pl\n"
"POT-Creation-Date: 2007-10-18 08:12-0700\n"
"PO-Revision-Date: 2007-10-18 19:33+0200\n"
"Last-Translator: Piotr Drąg <raven@pmail.pl>\n"
"Language-Team: Polish <pl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: entities-en_US.xml:4(title)
msgid ""
"These common entities are useful shorthand terms and names, which may be "
"subject to change at anytime. This is an important value the the entity "
"provides: a single location to update terms and common names."
msgstr ""
"Te wspólne jednostki są przydatnymi skróconymi terminami i nazwami, które "
"mogą być zmieniane w dowolnym czasie. To ważna wartość, jaką dostarczają "
"jednostki: pojedyncze położenie do aktualizowania terminów i wspólnych nazw."

#: entities-en_US.xml:7(comment) entities-en_US.xml:15(comment)
msgid "Generic root term"
msgstr "Ogólny termin root"

#: entities-en_US.xml:8(text)
msgid "Fedora"
msgstr "Fedora"

#: entities-en_US.xml:11(comment)
msgid "Generic root term in lowercase"
msgstr "Ogólny termin root zapisany małymi literami"

#: entities-en_US.xml:12(text)
msgid "fedora"
msgstr "fedora"

#: entities-en_US.xml:16(text)
msgid "Core"
msgstr "Core"

#: entities-en_US.xml:19(comment)
msgid "Generic main project name"
msgstr "Ogólna główna nazwa projektu"

#: entities-en_US.xml:23(comment)
msgid "Legacy Entity"
msgstr "Jednostka Legacy"

#: entities-en_US.xml:27(comment)
msgid "Short project name"
msgstr "Krótka nazwa projektu"

#: entities-en_US.xml:28(text)
msgid "F"
msgstr "F"

#: entities-en_US.xml:31(comment)
msgid "Generic overall project name"
msgstr "Ogólna nazwa projektu"

#: entities-en_US.xml:32(text)
msgid "<use entity=\"FED\"/> Project"
msgstr "Projekt <use entity=\"FED\"/>"

#: entities-en_US.xml:35(comment)
msgid "Generic docs project name"
msgstr "Ogólna nazwa projektu dokumentacji"

#: entities-en_US.xml:36(text)
msgid "<use entity=\"FED\"/> Documentation Project"
msgstr "Projekt dokumentacji <use entity=\"FED\"/>"

#: entities-en_US.xml:39(comment)
msgid "Short docs project name"
msgstr "Krótka nazwa projektu dokumentacji"

#: entities-en_US.xml:40(text)
msgid "<use entity=\"FED\"/> Docs Project"
msgstr "Projekt dokumentacji <use entity=\"FED\"/>"

#: entities-en_US.xml:43(comment)
msgid "cf. Core"
msgstr "cf. Core"

#: entities-en_US.xml:44(text)
msgid "Extras"
msgstr "Extras"

#: entities-en_US.xml:47(comment)
msgid "cf. Fedora Core"
msgstr "cf. Fedora Core"

#: entities-en_US.xml:51(comment)
msgid "Fedora Docs Project URL"
msgstr "URL projektu dokumentacji Fedory"

#: entities-en_US.xml:55(comment)
msgid "Fedora Project URL"
msgstr "URL projektu Fedora"

#: entities-en_US.xml:59(comment)
msgid "Fedora Documentation (repository) URL"
msgstr "URL dokumentacji Fedory (repozytorium)"

#: entities-en_US.xml:63(comment) entities-en_US.xml:64(text)
msgid "Bugzilla"
msgstr "Bugzilla"

#: entities-en_US.xml:67(comment)
msgid "Bugzilla URL"
msgstr "URL Bugzilli"

#: entities-en_US.xml:71(comment)
msgid "Bugzilla product for Fedora Docs"
msgstr "Produkt Bugzilli dla dokumentacji Fedory"

#: entities-en_US.xml:72(text)
msgid "<use entity=\"FED\"/> Documentation"
msgstr "Dokumentacja <use entity=\"FED\"/>"

#: entities-en_US.xml:77(comment)
msgid "Current release version of main project"
msgstr "Bieżąca wersja wydania głównego projektu"

#: entities-en_US.xml:78(text)
msgid "8"
msgstr "8"

#: entities-en_US.xml:81(comment)
msgid "Alias for FCVER"
msgstr "Alias dla FCVER"

#: entities-en_US.xml:85(comment)
msgid "Current test number of main project"
msgstr "Bieżący numer wydania testowego głównego projektu"

#: entities-en_US.xml:86(text)
msgid "Alpha"
msgstr "Alfa"

#: entities-en_US.xml:89(comment)
msgid "Current test version of main project"
msgstr "Bieżący testowa wersja głównego projektu"

#: entities-en_US.xml:90(text)
msgid "9 <use entity=\"TESTVER\"/>"
msgstr "9 <use entity=\"TESTVER\"/>"

#: entities-en_US.xml:93(comment)
msgid "Alias for FCTESTVER"
msgstr "Alias dla FCTESTVER"

#: entities-en_US.xml:99(comment)
msgid "DocBook version for primary use"
msgstr "Wersja DocBook do głównego użytku"

#: entities-en_US.xml:100(text)
msgid "4.4"
msgstr "4.4"

#: entities-en_US.xml:105(comment)
msgid "The generic term \"Red Hat\""
msgstr "Ogólny termin \"Red Hat\""

#: entities-en_US.xml:106(text)
msgid "Red Hat"
msgstr "Red Hat"

#: entities-en_US.xml:109(comment)
msgid "The generic term \"Red Hat, Inc.\""
msgstr "Ogólny termin \"Red Hat, Inc.\""

#: entities-en_US.xml:110(text)
msgid "<use entity=\"RH\"/> Inc."
msgstr "<use entity=\"RH\"/> Inc."

#: entities-en_US.xml:113(comment)
msgid "The generic term \"Red Hat Linux\""
msgstr "Ogólny termin \"Red Hat Linux\""

#: entities-en_US.xml:114(text)
msgid "<use entity=\"RH\"/> Linux"
msgstr "<use entity=\"RH\"/> Linux"

#: entities-en_US.xml:117(comment)
msgid "The generic term \"Red Hat Network\""
msgstr "Ogólny termin \"Red Hat Network\""

#: entities-en_US.xml:118(text)
msgid "<use entity=\"RH\"/> Network"
msgstr "<use entity=\"RH\"/> Network"

#: entities-en_US.xml:121(comment)
msgid "The generic term \"Red Hat Enterprise Linux\""
msgstr "Ogólny termin \"Red Hat Enterprise Linux\""

#: entities-en_US.xml:122(text)
msgid "<use entity=\"RH\"/> Enterprise Linux"
msgstr "<use entity=\"RH\"/> Enterprise Linux"

#: entities-en_US.xml:127(comment)
msgid "Generic technology term"
msgstr "Ogólny termin technologii"

#: entities-en_US.xml:128(text)
msgid "SELinux"
msgstr "SELinux"

#: entities-en_US.xml:133(comment)
msgid "Legal notice container"
msgstr "Kontener dla uwag o legalności"

#: entities-en_US.xml:134(text)
msgid "legalnotice-en_US.xml"
msgstr "legalnotice-pl.xml"

#: entities-en_US.xml:137(comment)
msgid "Legal notice content"
msgstr "Zawartość uwag o legalności"

#: entities-en_US.xml:138(text)
msgid "legalnotice-content-en_US.xml"
msgstr "legalnotice-content-pl.xml"

#: entities-en_US.xml:141(comment)
msgid "OPL legal notice container"
msgstr "Kontener dla uwag o legalności OPL"

#: entities-en_US.xml:142(text)
msgid "legalnotice-opl-en_US.xml"
msgstr "legalnotice-opl-pl.xml"

#: entities-en_US.xml:145(comment)
msgid "OPL legal notice content"
msgstr "Zawartość uwag o legalności OPL"

#: entities-en_US.xml:146(text)
msgid "opl.xml"
msgstr "opl.xml"

#: entities-en_US.xml:149(comment) entities-en_US.xml:153(comment)
msgid "OPL legal notice for relnotes"
msgstr "Uwagi o legalności dla Informacji o wydaniu"

#: entities-en_US.xml:150(text)
msgid "legalnotice-relnotes-en_US.xml"
msgstr "legalnotice-relnotes-pl.xml"

#: entities-en_US.xml:154(text)
msgid "legalnotice-section-en_US.xml"
msgstr "legalnotice-section-pl.xml"

#: entities-en_US.xml:157(comment)
msgid "Bug reporting tip"
msgstr "Podpowiedź o zgłaszaniu błędów"

#: entities-en_US.xml:158(text)
msgid "bugreporting-en_US.xml"
msgstr "bugreporting-pl.xml"

#: entities-en_US.xml:165(comment)
msgid "Name of project"
msgstr "Nazwa projektu"

#: entities-en_US.xml:171(comment)
msgid "Name of installation documentation"
msgstr "Nazwa dokumentacji instalacji"

#: entities-en_US.xml:172(text)
msgid "Installation Guide"
msgstr "Przewodnik po instalacji"

#: entities-en_US.xml:175(comment)
msgid "Name of documentation documentation"
msgstr "Nazwa Przewodnika po dokumentacji"

#: entities-en_US.xml:176(text)
msgid "Documentation Guide"
msgstr "Przewodnik po dokumentacji"

#: entities-en_US.xml:181(comment)
msgid "URL for IG"
msgstr "URL Przewodnika po instalacji"

#: entities-en_US.xml:185(comment)
msgid "URL for DocG"
msgstr "URL Przewodnika po dokumentacji"

#: entities-en_US.xml:191(comment)
msgid "Notice of draft content"
msgstr "Zawartość ostrzeżenia o szkicu"

#: entities-en_US.xml:192(text)
msgid "draftnotice-en_US.xml"
msgstr "draftnotice-pl.xml"

#: entities-en_US.xml:195(comment)
msgid "Notice of legacy content"
msgstr "Ostrzeżenie o przestarzałej zawartości"

#: entities-en_US.xml:196(text)
msgid "legacynotice-en_US.xml"
msgstr "legacynotice-pl.xml"

#: entities-en_US.xml:199(comment)
msgid "Notice of obsolescence"
msgstr "Ostrzeżenie o przestarzałej zawartości"

#: entities-en_US.xml:200(text)
msgid "obsoletenotice-en_US.xml"
msgstr "obsoletenotice-pl.xml"

#: entities-en_US.xml:203(comment)
msgid "Notice of deprecation"
msgstr "Ostrzeżenie o przestarzałej zawartości"

#: entities-en_US.xml:204(text)
msgid "deprecatednotice-en_US.xml"
msgstr "deprecatednotice-pl.xml"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2.
#: entities-en_US.xml:0(None)
msgid "translator-credits"
msgstr "Piotr Drąg <raven@pmail.pl>, 2006"
