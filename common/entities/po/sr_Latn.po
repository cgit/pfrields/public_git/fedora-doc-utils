# Serbian(Latin) translations for entities
#
# Igor Miletic <grejigl-gnomeprevod@yahoo.ca>, 2006.
# Miloš Komarčević <kmilos@gmail.com>, 2007.
# Nikola Pajtić <salgeras@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: entities\n"
"POT-Creation-Date: 2007-10-18 08:12-0700\n"
"PO-Revision-Date: 2008-03-14 18:22+0100\n"
"Last-Translator: Nikola Pajtić <salgeras@gmail.com>\n"
"Language-Team: Serbian (sr) <fedora-trans-sr@redhat.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: entities-en_US.xml:4(title)
msgid ""
"These common entities are useful shorthand terms and names, which may be "
"subject to change at anytime. This is an important value the the entity "
"provides: a single location to update terms and common names."
msgstr ""
"Ovi zajednički entiteti su korisne prečice i imena, koja mogu biti "
"promenjena u bilo koje vreme. Ovo je važna osobina koju entiteti pružaju: "
"jedno mesto za ažuririranje termina i zajedničkih imena."

#: entities-en_US.xml:7(comment) entities-en_US.xml:15(comment)
msgid "Generic root term"
msgstr "Opšti korenski termin"

#: entities-en_US.xml:8(text)
msgid "Fedora"
msgstr "Fedora"

#: entities-en_US.xml:11(comment)
msgid "Generic root term in lowercase"
msgstr "Opšti korenski termin malim slovima"

#: entities-en_US.xml:12(text)
msgid "fedora"
msgstr "fedora"

#: entities-en_US.xml:16(text)
msgid "Core"
msgstr "Core"

#: entities-en_US.xml:19(comment)
msgid "Generic main project name"
msgstr "Opšte glavno ime projekta"

#: entities-en_US.xml:23(comment)
msgid "Legacy Entity"
msgstr "Entitet naslednosti"

#: entities-en_US.xml:27(comment)
msgid "Short project name"
msgstr "Skraćeni naziv projekta"

#: entities-en_US.xml:28(text)
msgid "F"
msgstr "F"

#: entities-en_US.xml:31(comment)
msgid "Generic overall project name"
msgstr "Opšte ukupno ime projekta"

#: entities-en_US.xml:32(text)
msgid "<use entity=\"FED\"/> Project"
msgstr "<use entity=\"FED\"/> projekat"

#: entities-en_US.xml:35(comment)
msgid "Generic docs project name"
msgstr "Opšte ime projekta dokumentacije"

#: entities-en_US.xml:36(text)
msgid "<use entity=\"FED\"/> Documentation Project"
msgstr "<use entity=\"FED\"/> projekat dokumentacije"

#: entities-en_US.xml:39(comment)
msgid "Short docs project name"
msgstr "Skraćeno ime projekta dokumentacije"

#: entities-en_US.xml:40(text)
msgid "<use entity=\"FED\"/> Docs Project"
msgstr "<use entity=\"FED\"/> dok. projekat"

#: entities-en_US.xml:43(comment)
msgid "cf. Core"
msgstr "vidi Core"

#: entities-en_US.xml:44(text)
msgid "Extras"
msgstr "Extras"

#: entities-en_US.xml:47(comment)
msgid "cf. Fedora Core"
msgstr "vidi Fedora Core"

#: entities-en_US.xml:51(comment)
msgid "Fedora Docs Project URL"
msgstr "URL projekta Fedora dokumentacije"

#: entities-en_US.xml:55(comment)
msgid "Fedora Project URL"
msgstr "URL Fedora projekta"

#: entities-en_US.xml:59(comment)
msgid "Fedora Documentation (repository) URL"
msgstr "URL (riznice) Fedora dokumentacije"

#: entities-en_US.xml:63(comment) entities-en_US.xml:64(text)
msgid "Bugzilla"
msgstr "Bugzilla"

#: entities-en_US.xml:67(comment)
msgid "Bugzilla URL"
msgstr "Bugzilla URL"

#: entities-en_US.xml:71(comment)
msgid "Bugzilla product for Fedora Docs"
msgstr "Bugzilla proizvod za Fedora dokumentaciju"

#: entities-en_US.xml:72(text)
msgid "<use entity=\"FED\"/> Documentation"
msgstr "<use entity=\"FED\"/> dokumentacija"

#: entities-en_US.xml:77(comment)
msgid "Current release version of main project"
msgstr "Trenutna verzija izdanja glavnog projekta"

#: entities-en_US.xml:78(text)
msgid "8"
msgstr "8"

#: entities-en_US.xml:81(comment)
msgid "Alias for FCVER"
msgstr "Pseudonim za FCVER"

#: entities-en_US.xml:85(comment)
msgid "Current test number of main project"
msgstr "Trenutni probni broj glavnog projekta"

#: entities-en_US.xml:86(text)
msgid "Alpha"
msgstr "alfa"

#: entities-en_US.xml:89(comment)
msgid "Current test version of main project"
msgstr "Trenutna probna verzija test projekta"

#: entities-en_US.xml:90(text)
msgid "9 <use entity=\"TESTVER\"/>"
msgstr "9 <use entity=\"TESTVER\"/>"

#: entities-en_US.xml:93(comment)
msgid "Alias for FCTESTVER"
msgstr "Pseudonim za FCTESTVER"

#: entities-en_US.xml:99(comment)
msgid "DocBook version for primary use"
msgstr "DocBook verzija za osnovnu upotrebu"

#: entities-en_US.xml:100(text)
msgid "4.4"
msgstr "4.4"

#: entities-en_US.xml:105(comment)
msgid "The generic term \"Red Hat\""
msgstr "Opšti termin „Red Hat“"

#: entities-en_US.xml:106(text)
msgid "Red Hat"
msgstr "Red Hat"

#: entities-en_US.xml:109(comment)
msgid "The generic term \"Red Hat, Inc.\""
msgstr "Opšti termin „Red Hat, Inc.“"

#: entities-en_US.xml:110(text)
msgid "<use entity=\"RH\"/> Inc."
msgstr "<use entity=\"RH\"/> Inc."

#: entities-en_US.xml:113(comment)
msgid "The generic term \"Red Hat Linux\""
msgstr "Opšti termin „Red Hat Linux“"

#: entities-en_US.xml:114(text)
msgid "<use entity=\"RH\"/> Linux"
msgstr "<use entity=\"RH\"/> Linux"

#: entities-en_US.xml:117(comment)
msgid "The generic term \"Red Hat Network\""
msgstr "Opšti termin „Red Hat Network“"

#: entities-en_US.xml:118(text)
msgid "<use entity=\"RH\"/> Network"
msgstr "<use entity=\"RH\"/> Network"

#: entities-en_US.xml:121(comment)
msgid "The generic term \"Red Hat Enterprise Linux\""
msgstr "Opšti termin „Red Hat Enterprise Linux“"

#: entities-en_US.xml:122(text)
msgid "<use entity=\"RH\"/> Enterprise Linux"
msgstr "<use entity=\"RH\"/> Enterprise Linux"

#: entities-en_US.xml:127(comment)
msgid "Generic technology term"
msgstr "Opšti tehnološki termin"

#: entities-en_US.xml:128(text)
msgid "SELinux"
msgstr "SELinux"

#: entities-en_US.xml:133(comment)
msgid "Legal notice container"
msgstr "Držač za zakonska obaveštenja"

#: entities-en_US.xml:134(text)
msgid "legalnotice-en_US.xml"
msgstr "legalnotice-sr_Latn.xml"

#: entities-en_US.xml:137(comment)
msgid "Legal notice content"
msgstr "Obaveštenje o zakonskom sadržaju"

#: entities-en_US.xml:138(text)
msgid "legalnotice-content-en_US.xml"
msgstr "legalnotice-content-sr_Latn.xml"

#: entities-en_US.xml:141(comment)
msgid "OPL legal notice container"
msgstr "Držač za OPL zakonska obaveštenja"

#: entities-en_US.xml:142(text)
msgid "legalnotice-opl-en_US.xml"
msgstr "legalnotice-opl-sr_Latn.xml"

#: entities-en_US.xml:145(comment)
msgid "OPL legal notice content"
msgstr "Obaveštenje o zakonskom sadržaju u OPL"

#: entities-en_US.xml:146(text)
msgid "opl.xml"
msgstr "opl.xml"

#: entities-en_US.xml:149(comment) entities-en_US.xml:153(comment)
msgid "OPL legal notice for relnotes"
msgstr "Obaveštenje o zakonskom sadržaju u OPL-u za beleške o izdanju"

#: entities-en_US.xml:150(text)
msgid "legalnotice-relnotes-en_US.xml"
msgstr "legalnotice-relnotes-sr_Latn.xml"

#: entities-en_US.xml:154(text)
msgid "legalnotice-section-en_US.xml"
msgstr "legalnotice-section-sr_Latn.xml"

#: entities-en_US.xml:157(comment)
msgid "Bug reporting tip"
msgstr "Savet za izveštavanje o greškama"

#: entities-en_US.xml:158(text)
msgid "bugreporting-en_US.xml"
msgstr "bugreporting-sr_Latn.xml"

#: entities-en_US.xml:165(comment)
msgid "Name of project"
msgstr "Ime projekta"

#: entities-en_US.xml:171(comment)
msgid "Name of installation documentation"
msgstr "Ime instalacione dokumentacije"

#: entities-en_US.xml:172(text)
msgid "Installation Guide"
msgstr "Vodič kroz instalaciju"

#: entities-en_US.xml:175(comment)
msgid "Name of documentation documentation"
msgstr "Ime dokumentacijske dokumentacije"

#: entities-en_US.xml:176(text)
msgid "Documentation Guide"
msgstr "Vodič kroz dokumentaciju"

#: entities-en_US.xml:181(comment)
msgid "URL for IG"
msgstr "URL za IV"

#: entities-en_US.xml:185(comment)
msgid "URL for DocG"
msgstr "URL za DokV"

#: entities-en_US.xml:191(comment)
msgid "Notice of draft content"
msgstr "Obaveštenje o sadržaju nacrta"

#: entities-en_US.xml:192(text)
msgid "draftnotice-en_US.xml"
msgstr "draftnotice-sr_Latn.xml"

#: entities-en_US.xml:195(comment)
msgid "Notice of legacy content"
msgstr "Obaveštenje o zastarelom sadržaju"

#: entities-en_US.xml:196(text)
msgid "legacynotice-en_US.xml"
msgstr "legacynotice-sr_Latn.xml"

#: entities-en_US.xml:199(comment)
msgid "Notice of obsolescence"
msgstr "Obaveštenje o prevaziđenosti"

#: entities-en_US.xml:200(text)
msgid "obsoletenotice-en_US.xml"
msgstr "obsoletenotice-sr_Latn.xml"

#: entities-en_US.xml:203(comment)
msgid "Notice of deprecation"
msgstr "Obaveštenje o odbacivanju"

#: entities-en_US.xml:204(text)
msgid "deprecatednotice-en_US.xml"
msgstr "deprecatednotice-sr_Latn.xml"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2.
#: entities-en_US.xml:0(None)
msgid "translator-credits"
msgstr ""
"Igor Miletić <grejigl-gnomeprevod@yahoo.ca>, 2006, 2007.\n"
"Miloš Komarčević <kmilos@gmail.com>, 2007.\n"
"Nikola Pajtić <salgeras@gmail.com>, 2008."
