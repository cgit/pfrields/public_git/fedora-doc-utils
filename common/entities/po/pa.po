# translation of pa.po to Punjabi
# Amanpreet Singh Alam <apbrar@gmail.com>, 2006.
# A S Alam <apbrar@gmail.com>, 2006.
# A S Alam <aalam@users.sf.net>, 2007.
# Jaswinder Singh <jsingh@redhat.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: pa\n"
"POT-Creation-Date: 2007-10-18 08:12-0700\n"
"PO-Revision-Date: 2007-07-30 12:25+0530\n"
"Last-Translator: Jaswinder Singh <jsingh@redhat.com>\n"
"Language-Team: Punjabi <fedora-trans-pa@redhat.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: entities-en_US.xml:4(title)
msgid ""
"These common entities are useful shorthand terms and names, which may be "
"subject to change at anytime. This is an important value the the entity "
"provides: a single location to update terms and common names."
msgstr ""
"ਇਹ ਆਮ ਸ਼ਬਦ ਛੋਟੇ ਰੂਪ ਵਿੱਚ ਲਿਖਣ ਲਈ ਸਹਾਇਕ ਹਨ, ਜਿੱਥੇ ਕਿ ਵਿਸ਼ਾ ਕਿਸੇ ਵੀ ਸਮੇਂ ਬਦਲਿਆ ਜਾ ਸਕਦਾ ਹੈ। "
"ਇਹ ਖਾਸ ਮੁੱਲ ਹੈ, ਜੋ ਕਿ ਇੱਕ ਇੰਦਰਾਜ਼ ਦਿੰਦਾ ਹੈ: ਜਿਸ ਨਾਲ ਸ਼ਬਦਾਂ ਅਤੇ ਆਮ ਨਾਵਾਂ ਨੂੰ ਇੱਕੋ ਥਾਂ ਤੋਂ ਅੱਪਡੇਟ "
"ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ।"

#: entities-en_US.xml:7(comment) entities-en_US.xml:15(comment)
msgid "Generic root term"
msgstr "ਆਮ ਰੂਟ ਸ਼ਬਦ"

#: entities-en_US.xml:8(text)
msgid "Fedora"
msgstr "ਫੇਡੋਰਾ"

#: entities-en_US.xml:11(comment)
msgid "Generic root term in lowercase"
msgstr "ਛੋਟੇ ਅੱਖਰਾਂ ਵਿੱਚ ਆਮ ਰੂਟ ਸ਼ਬਦ"

#: entities-en_US.xml:12(text)
msgid "fedora"
msgstr "ਫੇਡੋਰਾ"

#: entities-en_US.xml:16(text)
msgid "Core"
msgstr "ਕੋਰ"

#: entities-en_US.xml:19(comment)
msgid "Generic main project name"
msgstr "ਆਮ ਮੁੱਖ ਪਰੋਜੈੱਕਟ ਨਾਂ"

#: entities-en_US.xml:23(comment)
msgid "Legacy Entity"
msgstr "ਪੁਰਾਤਨ ਇੰਦਰਾਜ਼"

#: entities-en_US.xml:27(comment)
msgid "Short project name"
msgstr "ਛੋਟਾ ਪਰੋਜੈੱਕਟ ਨਾਂ"

#: entities-en_US.xml:28(text)
msgid "F"
msgstr "F"

#: entities-en_US.xml:31(comment)
msgid "Generic overall project name"
msgstr "ਆਮ ਮੁੱਖ ਪਰੋਜੈੱਕਟ ਨਾਂ"

#: entities-en_US.xml:32(text)
msgid "<use entity=\"FED\"/> Project"
msgstr "<use entity=\"FED\"/> ਪਰੋਜੈੱਕਟ"

#: entities-en_US.xml:35(comment)
msgid "Generic docs project name"
msgstr "ਆਮ ਦਸਤਾਵੇਜ਼ ਪਰੋਜੈੱਕਟ ਨਾਂ"

#: entities-en_US.xml:36(text)
msgid "<use entity=\"FED\"/> Documentation Project"
msgstr "<use entity=\"FED\"/> ਦਸਤਾਵੇਜ਼ ਪਰੋਜੈੱਕਟ"

#: entities-en_US.xml:39(comment)
msgid "Short docs project name"
msgstr "ਛੋਟਾ ਦਸਤਾਵੇਜ਼ ਪਰੋਜੈੱਕਟ ਨਾਂ"

#: entities-en_US.xml:40(text)
msgid "<use entity=\"FED\"/> Docs Project"
msgstr "<use entity=\"FED\"/> ਦਸਤਾਵੇਜ਼ ਪਰੋਜੈੱਕਟ"

#: entities-en_US.xml:43(comment)
msgid "cf. Core"
msgstr "cf. ਕੋਰ"

#: entities-en_US.xml:44(text)
msgid "Extras"
msgstr "ਐਕਸਟਰਾ"

#: entities-en_US.xml:47(comment)
msgid "cf. Fedora Core"
msgstr "cf. ਫੇਡੋਰਾ ਕੋਰ"

#: entities-en_US.xml:51(comment)
msgid "Fedora Docs Project URL"
msgstr "ਫੇਡੋਰਾ ਦਸਤਾਵੇਜ਼ ਪਰੋਜੈੱਕਟ URL"

#: entities-en_US.xml:55(comment)
msgid "Fedora Project URL"
msgstr "ਫੇਡੋਰਾ ਪਰੋਜੈੱਕਟ URL"

#: entities-en_US.xml:59(comment)
msgid "Fedora Documentation (repository) URL"
msgstr "ਫੇਡੋਰਾ ਦਸਤਾਵੇਜ਼ (ਰਿਪੋਜ਼ਟਰੀ) URL"

#: entities-en_US.xml:63(comment) entities-en_US.xml:64(text)
msgid "Bugzilla"
msgstr "ਬੱਗਜ਼ੀਲਾ"

#: entities-en_US.xml:67(comment)
msgid "Bugzilla URL"
msgstr "ਬੱਗਜ਼ੀਲਾ URL"

#: entities-en_US.xml:71(comment)
msgid "Bugzilla product for Fedora Docs"
msgstr "ਫੇਡੋਰਾ ਦਸਤਾਵੇਜ਼ਾਂ ਲਈ ਬੱਗਜ਼ੀਲਾ ਉਤਪਾਦ"

#: entities-en_US.xml:72(text)
msgid "<use entity=\"FED\"/> Documentation"
msgstr "<use entity=\"FED\"/> ਦਸਤਾਵੇਜ਼"

#: entities-en_US.xml:77(comment)
msgid "Current release version of main project"
msgstr "ਮੁੱਖ ਪਰੋਜੈੱਕਟ ਦਾ ਮੌਜੂਦਾ ਜਾਰੀ ਵਰਜਨ"

#: entities-en_US.xml:78(text)
msgid "8"
msgstr ""

#: entities-en_US.xml:81(comment)
msgid "Alias for FCVER"
msgstr "FCVER ਲਈ ਉਪ-ਨਾਂ"

#: entities-en_US.xml:85(comment)
msgid "Current test number of main project"
msgstr "ਮੁੱਖ ਪਰੋਜੈੱਕਟ ਦਾ ਮੌਜੂਦਾ ਟੈਸਟ ਨੰਬਰ"

#: entities-en_US.xml:86(text)
msgid "Alpha"
msgstr ""

#: entities-en_US.xml:89(comment)
msgid "Current test version of main project"
msgstr "ਮੁੱਖ ਪਰੋਜੈੱਕਟ ਦਾ ਮੌਜੂਦਾ ਟੈਸਟ ਵਰਜਨ"

#: entities-en_US.xml:90(text)
#, fuzzy
msgid "9 <use entity=\"TESTVER\"/>"
msgstr "7 <use entity=\"TESTVER\"/>"

#: entities-en_US.xml:93(comment)
msgid "Alias for FCTESTVER"
msgstr "FCTESTVER ਲਈ ਉਪ-ਨਾਂ"

#: entities-en_US.xml:99(comment)
msgid "DocBook version for primary use"
msgstr "ਆਮ ਵਰਤੋਂ ਲਈ DocBook ਵਰਜਨ"

#: entities-en_US.xml:100(text)
msgid "4.4"
msgstr "4.4"

#: entities-en_US.xml:105(comment)
msgid "The generic term \"Red Hat\""
msgstr "ਆਮ ਸ਼ਬਦ \"Red Hat\""

#: entities-en_US.xml:106(text)
msgid "Red Hat"
msgstr "Red Hat"

#: entities-en_US.xml:109(comment)
msgid "The generic term \"Red Hat, Inc.\""
msgstr "ਆਮ ਸ਼ਬਦ \"Red Hat, Inc.\""

#: entities-en_US.xml:110(text)
msgid "<use entity=\"RH\"/> Inc."
msgstr "<use entity=\"RH\"/> Inc."

#: entities-en_US.xml:113(comment)
msgid "The generic term \"Red Hat Linux\""
msgstr "ਆਮ ਸ਼ਬਦ \"Red Hat Linux\""

#: entities-en_US.xml:114(text)
msgid "<use entity=\"RH\"/> Linux"
msgstr "<use entity=\"RH\"/> Linux"

#: entities-en_US.xml:117(comment)
msgid "The generic term \"Red Hat Network\""
msgstr "ਆਮ ਸ਼ਬਦ \"Red Hat Network\""

#: entities-en_US.xml:118(text)
msgid "<use entity=\"RH\"/> Network"
msgstr "<use entity=\"RH\"/> Network"

#: entities-en_US.xml:121(comment)
msgid "The generic term \"Red Hat Enterprise Linux\""
msgstr "ਆਮ ਸ਼ਬਦ \"Red Hat Enterprise Linux\""

#: entities-en_US.xml:122(text)
msgid "<use entity=\"RH\"/> Enterprise Linux"
msgstr "<use entity=\"RH\"/> Enterprise Linux"

#: entities-en_US.xml:127(comment)
msgid "Generic technology term"
msgstr "ਆਮ ਤਕਨਾਲੋਜੀ ਸ਼ਬਦ"

#: entities-en_US.xml:128(text)
msgid "SELinux"
msgstr "SELinux"

#: entities-en_US.xml:133(comment)
msgid "Legal notice container"
msgstr "ਲੀਗਲ ਨੋਟਿਸ ਕੰਨਟੇਨਰ"

#: entities-en_US.xml:134(text)
msgid "legalnotice-en_US.xml"
msgstr "legalnotice-pa.xml"

#: entities-en_US.xml:137(comment)
msgid "Legal notice content"
msgstr "ਲੀਗਲ ਨੋਟਿਸ ਸਮੱਗਰੀ"

#: entities-en_US.xml:138(text)
msgid "legalnotice-content-en_US.xml"
msgstr "legalnotice-content-pa.xml"

#: entities-en_US.xml:141(comment)
msgid "OPL legal notice container"
msgstr "OPL ਲੀਗਲ ਨੋਟਿਸ ਕੰਨਟੇਨਰ"

#: entities-en_US.xml:142(text)
msgid "legalnotice-opl-en_US.xml"
msgstr "legalnotice-opl-pa.xml"

#: entities-en_US.xml:145(comment)
msgid "OPL legal notice content"
msgstr "OPL ਲੀਗਲ ਨੋਟਿਸ ਸਮੱਗਰੀ"

#: entities-en_US.xml:146(text)
msgid "opl.xml"
msgstr "opl.xml"

#: entities-en_US.xml:149(comment) entities-en_US.xml:153(comment)
msgid "OPL legal notice for relnotes"
msgstr "ਰੀਲਿਜ਼ ਨੋਟਿਸ ਲਈ OPL ਲੀਗਲ ਨੋਟਿਸ"

#: entities-en_US.xml:150(text)
msgid "legalnotice-relnotes-en_US.xml"
msgstr "legalnotice-relnotes-pa.xml"

#: entities-en_US.xml:154(text)
msgid "legalnotice-section-en_US.xml"
msgstr "legalnotice-section-pa.xml"

#: entities-en_US.xml:157(comment)
msgid "Bug reporting tip"
msgstr "ਬੱਗ ਰਿਪੋਟਿੰਗ ਇਸ਼ਾਰੇ"

#: entities-en_US.xml:158(text)
msgid "bugreporting-en_US.xml"
msgstr "bugreporting-pa.xml"

#: entities-en_US.xml:165(comment)
msgid "Name of project"
msgstr "ਪ੍ਰੋਜੈਕਟ ਲਈ ਨਾਂ"

#: entities-en_US.xml:171(comment)
msgid "Name of installation documentation"
msgstr "ਇੰਸਟਾਲੇਸ਼ਨ ਦਸਤਾਵੇਜ਼ ਲਈ ਨਾਂ"

#: entities-en_US.xml:172(text)
msgid "Installation Guide"
msgstr "ਇੰਸਟਾਲੇਸ਼ਨ ਗਾਈਡ"

#: entities-en_US.xml:175(comment)
msgid "Name of documentation documentation"
msgstr "ਦਸਤਾਵੇਜ਼ ਲਈ ਨਾਂ"

#: entities-en_US.xml:176(text)
msgid "Documentation Guide"
msgstr "ਦਸਤਾਵੇਜ਼ ਗਾਈਡ"

#: entities-en_US.xml:181(comment)
msgid "URL for IG"
msgstr "IG ਲਈ URL"

#: entities-en_US.xml:185(comment)
msgid "URL for DocG"
msgstr "DocG ਲਈ URL"

#: entities-en_US.xml:191(comment)
msgid "Notice of draft content"
msgstr "ਡਰਾਫਟ ਸਮੱਗਰੀ ਲਈ ਨੋਟਿਸ"

#: entities-en_US.xml:192(text)
msgid "draftnotice-en_US.xml"
msgstr "draftnotice-pa.xml"

#: entities-en_US.xml:195(comment)
msgid "Notice of legacy content"
msgstr "ਪੁਰਾਤਨ ਸਮੱਗਰੀ ਲਈ ਨੋਟਿਸ"

#: entities-en_US.xml:196(text)
msgid "legacynotice-en_US.xml"
msgstr "legacynotice-pa.xml"

#: entities-en_US.xml:199(comment)
msgid "Notice of obsolescence"
msgstr "ਹਟਾਉਣ ਲਈ ਨੋਟਿਸ"

#: entities-en_US.xml:200(text)
msgid "obsoletenotice-en_US.xml"
msgstr "obsoletenotice-pa.xml"

#: entities-en_US.xml:203(comment)
msgid "Notice of deprecation"
msgstr "ਬਰਤਰਫ਼ ਲਈ ਨੋਟਿਸ"

#: entities-en_US.xml:204(text)
msgid "deprecatednotice-en_US.xml"
msgstr "deprecatednotice-pa.xml"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2.
#: entities-en_US.xml:0(None)
msgid "translator-credits"
msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ <aalam@users.sf.net>, 2006, 2007"

#~ msgid "7"
#~ msgstr "7"

#~ msgid "test4"
#~ msgstr "test4"
