# translation of el.po to Greek
# Dimitris Glezos <dimitris@glezos.com>, 2006, 2007.
# translation of el.po to
msgid ""
msgstr ""
"Project-Id-Version: el\n"
"POT-Creation-Date: 2007-10-18 08:12-0700\n"
"PO-Revision-Date: 2007-09-17 22:12+0100\n"
"Last-Translator: Dimitris Glezos <dimitris@glezos.com>\n"
"Language-Team: Greek <fedora-trans-el@redhat.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: entities-en_US.xml:4(title)
msgid ""
"These common entities are useful shorthand terms and names, which may be "
"subject to change at anytime. This is an important value the the entity "
"provides: a single location to update terms and common names."
msgstr ""
"Αυτές οι κοινές οντότητες είναι χρήσιμοι συντομογραφικοί όροι και ονόματα, "
"τα οποία μπορεί να αλλάξουν οποτεδήποτε. Αυτή είναι μια σημαντική αξία που "
"παρέχεται από την οντότητα: μια μοναδική τοποθεσία για ενημέρωση όρων και "
"κοινών ονομάτων."

#: entities-en_US.xml:7(comment) entities-en_US.xml:15(comment)
msgid "Generic root term"
msgstr "Γενικός αρχικός όρος"

#: entities-en_US.xml:8(text)
msgid "Fedora"
msgstr "Fedora"

#: entities-en_US.xml:11(comment)
msgid "Generic root term in lowercase"
msgstr "Γενικός αρχικός όρος με πεζά γράμματα"

#: entities-en_US.xml:12(text)
msgid "fedora"
msgstr "fedora"

#: entities-en_US.xml:16(text)
msgid "Core"
msgstr "Core"

#: entities-en_US.xml:19(comment)
msgid "Generic main project name"
msgstr "Γενικό όνομα κύριου έργου"

#: entities-en_US.xml:23(comment)
msgid "Legacy Entity"
msgstr "Παλιά οντότητα"

#: entities-en_US.xml:27(comment)
msgid "Short project name"
msgstr "Σύντομο όνομα έργου"

#: entities-en_US.xml:28(text)
msgid "F"
msgstr "F"

#: entities-en_US.xml:31(comment)
msgid "Generic overall project name"
msgstr "Γενικό όνομα συνολικού έργου"

#: entities-en_US.xml:32(text)
msgid "<use entity=\"FED\"/> Project"
msgstr "Έργο <use entity=\"FED\"/>"

#: entities-en_US.xml:35(comment)
msgid "Generic docs project name"
msgstr "Γενικό όνομα έργου τεκμηρίωσης"

#: entities-en_US.xml:36(text)
msgid "<use entity=\"FED\"/> Documentation Project"
msgstr "Έργο τεκμηρίωσης <use entity=\"FED\"/>"

#: entities-en_US.xml:39(comment)
msgid "Short docs project name"
msgstr "Σύντομο όνομα έργου τεκμηρίωσης"

#: entities-en_US.xml:40(text)
msgid "<use entity=\"FED\"/> Docs Project"
msgstr "Έργο τεκμηρίωσης <use entity=\"FED\"/>"

#: entities-en_US.xml:43(comment)
msgid "cf. Core"
msgstr "cf. Core"

#: entities-en_US.xml:44(text)
msgid "Extras"
msgstr "Extras"

#: entities-en_US.xml:47(comment)
msgid "cf. Fedora Core"
msgstr "cf. Fedora Core"

#: entities-en_US.xml:51(comment)
msgid "Fedora Docs Project URL"
msgstr "URL Έργου τεκμηρίωσης του Fedora"

#: entities-en_US.xml:55(comment)
msgid "Fedora Project URL"
msgstr "URL Έργου Fedora"

#: entities-en_US.xml:59(comment)
msgid "Fedora Documentation (repository) URL"
msgstr "URL Τεκμηρίωσης Fedora (αποθετηρίου)"

#: entities-en_US.xml:63(comment) entities-en_US.xml:64(text)
msgid "Bugzilla"
msgstr "Bugzilla"

#: entities-en_US.xml:67(comment)
msgid "Bugzilla URL"
msgstr "URL Bugzilla"

#: entities-en_US.xml:71(comment)
msgid "Bugzilla product for Fedora Docs"
msgstr "Προϊόν Bugzilla για Τεκμηρίωση Fedora"

#: entities-en_US.xml:72(text)
msgid "<use entity=\"FED\"/> Documentation"
msgstr "Τεκμηρίωση <use entity=\"FED\"/>"

#: entities-en_US.xml:77(comment)
msgid "Current release version of main project"
msgstr "Τρέχουσα έκδοση κυκλοφορίας κύριου έργου"

#: entities-en_US.xml:78(text)
msgid "8"
msgstr ""

#: entities-en_US.xml:81(comment)
msgid "Alias for FCVER"
msgstr "Συντόμευση για FCVER"

#: entities-en_US.xml:85(comment)
msgid "Current test number of main project"
msgstr "Τρέχον δοκιμαστικός αριθμός κύριου έργου"

#: entities-en_US.xml:86(text)
msgid "Alpha"
msgstr ""

#: entities-en_US.xml:89(comment)
msgid "Current test version of main project"
msgstr "Τρέχουσα δοκιμαστική έκδοση κύριου έργου"

#: entities-en_US.xml:90(text)
#, fuzzy
msgid "9 <use entity=\"TESTVER\"/>"
msgstr "7 <use entity=\"TESTVER\"/>"

#: entities-en_US.xml:93(comment)
msgid "Alias for FCTESTVER"
msgstr "Συντόμευση για FCTESTVER"

#: entities-en_US.xml:99(comment)
msgid "DocBook version for primary use"
msgstr "Έκδοση DocBook για πρωταρχική χρήση"

#: entities-en_US.xml:100(text)
msgid "4.4"
msgstr "4.4"

#: entities-en_US.xml:105(comment)
msgid "The generic term \"Red Hat\""
msgstr "Ο τυπικός όρος \"Red Hat\""

#: entities-en_US.xml:106(text)
msgid "Red Hat"
msgstr "Red Hat"

#: entities-en_US.xml:109(comment)
msgid "The generic term \"Red Hat, Inc.\""
msgstr "Ο τυπικός όρος \"Red Hat, Inc.\""

#: entities-en_US.xml:110(text)
msgid "<use entity=\"RH\"/> Inc."
msgstr "<use entity=\"RH\"/> Inc."

#: entities-en_US.xml:113(comment)
msgid "The generic term \"Red Hat Linux\""
msgstr "Ο τυπικός όρος \"Red Hat Linux\""

#: entities-en_US.xml:114(text)
msgid "<use entity=\"RH\"/> Linux"
msgstr "<use entity=\"RH\"/> Linux"

#: entities-en_US.xml:117(comment)
msgid "The generic term \"Red Hat Network\""
msgstr "Ο τυπικός όρος \"Red Hat Network\""

#: entities-en_US.xml:118(text)
msgid "<use entity=\"RH\"/> Network"
msgstr "<use entity=\"RH\"/> Network"

#: entities-en_US.xml:121(comment)
msgid "The generic term \"Red Hat Enterprise Linux\""
msgstr "Ο τυπικός όρος \"Red Hat Enterprise Linux\""

#: entities-en_US.xml:122(text)
msgid "<use entity=\"RH\"/> Enterprise Linux"
msgstr "<use entity=\"RH\"/> Enterprise Linux"

#: entities-en_US.xml:127(comment)
msgid "Generic technology term"
msgstr "Γενικός τεχνολογικός όρος"

#: entities-en_US.xml:128(text)
msgid "SELinux"
msgstr "SELinux"

#: entities-en_US.xml:133(comment)
msgid "Legal notice container"
msgstr "Υποδοχέας νομικής ειδοποίησης"

#: entities-en_US.xml:134(text)
msgid "legalnotice-en_US.xml"
msgstr "legalnotice-el.xml"

#: entities-en_US.xml:137(comment)
msgid "Legal notice content"
msgstr "Περιεχόμενο νομικής ειδοποίησης"

#: entities-en_US.xml:138(text)
msgid "legalnotice-content-en_US.xml"
msgstr "legalnotice-content-el.xml"

#: entities-en_US.xml:141(comment)
msgid "OPL legal notice container"
msgstr "Υποδοχέας νομικής ειδοποίησης OPL"

#: entities-en_US.xml:142(text)
msgid "legalnotice-opl-en_US.xml"
msgstr "legalnotice-opl-el.xml"

#: entities-en_US.xml:145(comment)
msgid "OPL legal notice content"
msgstr "Περιεχόμενο νομικής ειδοποίησης OPL"

#: entities-en_US.xml:146(text)
msgid "opl.xml"
msgstr "opl.xml"

#: entities-en_US.xml:149(comment) entities-en_US.xml:153(comment)
msgid "OPL legal notice for relnotes"
msgstr "Νομική ειδοποίηση OPL για τα relnotes"

#: entities-en_US.xml:150(text)
msgid "legalnotice-relnotes-en_US.xml"
msgstr "legalnotice-relnotes-el.xml"

#: entities-en_US.xml:154(text)
msgid "legalnotice-section-en_US.xml"
msgstr "legalnotice-section-el.xml"

#: entities-en_US.xml:157(comment)
msgid "Bug reporting tip"
msgstr "Συμβουλή αναφοράς σφαλμάτων"

#: entities-en_US.xml:158(text)
msgid "bugreporting-en_US.xml"
msgstr "bugreporting-el.xml"

#: entities-en_US.xml:165(comment)
msgid "Name of project"
msgstr "Όνομα του έργου"

#: entities-en_US.xml:171(comment)
msgid "Name of installation documentation"
msgstr "Όνομα της τεκμηρίωσης εγκατάστασης"

#: entities-en_US.xml:172(text)
msgid "Installation Guide"
msgstr "Οδηγός εγκατάστασης"

#: entities-en_US.xml:175(comment)
msgid "Name of documentation documentation"
msgstr "Όνομα της τεκμηρίωσης της τεκμηρίωσης"

#: entities-en_US.xml:176(text)
msgid "Documentation Guide"
msgstr "Οδηγός τεκμηρίωσης"

#: entities-en_US.xml:181(comment)
msgid "URL for IG"
msgstr "URL για τον IG"

#: entities-en_US.xml:185(comment)
msgid "URL for DocG"
msgstr "URL για τον DocG"

#: entities-en_US.xml:191(comment)
msgid "Notice of draft content"
msgstr "Ειδοποίηση για πρόχειρο περιεχόμενο"

#: entities-en_US.xml:192(text)
msgid "draftnotice-en_US.xml"
msgstr "draftnotice-el.xml"

#: entities-en_US.xml:195(comment)
msgid "Notice of legacy content"
msgstr "Ειδοποίηση για παλιό περιεχόμενο"

#: entities-en_US.xml:196(text)
msgid "legacynotice-en_US.xml"
msgstr "legacynotice-el.xml"

#: entities-en_US.xml:199(comment)
msgid "Notice of obsolescence"
msgstr "Ειδοποίηση για παρωχημένο υλικό"

#: entities-en_US.xml:200(text)
msgid "obsoletenotice-en_US.xml"
msgstr "obsoletenotice-el.xml"

#: entities-en_US.xml:203(comment)
msgid "Notice of deprecation"
msgstr "Ειδοποίηση για υλικό υπό κατάργηση"

#: entities-en_US.xml:204(text)
msgid "deprecatednotice-en_US.xml"
msgstr "deprecatednotice-el.xml"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2.
#: entities-en_US.xml:0(None)
msgid "translator-credits"
msgstr "Dimitris Glezos <dimitris@glezos.com>, 2007"

#~ msgid "7"
#~ msgstr "7"

#~ msgid "test4"
#~ msgstr "test4"
