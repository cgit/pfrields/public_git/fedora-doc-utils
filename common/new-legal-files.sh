#!/bin/bash
########################################################################
# Use this script to create the files needed to add support for a new
# locale.  To keep life simple, we simply copy the canonical "*-en_US.*"
# file content into a new "*-${LANG}.*" file set.
#
# TRANSLATORS NOTE: Legal notices require the assistance of a legal
# expert who is familiar with the copyright laws for the given locale.
# IT IS NOT PERMITTED TO SIMPLY TRANSLITERATE THE "en_US" FILE CONTENT
# INTO THE NEW LOCALE; THIS WILL MEAN ABSOLUTELY NOTHING LEGALLY.
########################################################################
# DEBUG=echo
initFile()	{
	dest=$(echo $1 | sed "s/-en_US/-${L}/")
	${DEBUG} cp $1 ${dest}
	${DEBUG} sed -i -e "s/-en_US/-${L}/g" ${dest}
	echo ${dest}
}
if [ $# -eq 0 ]; then
	echo "Usage: $0 locale ..." >&2
	exit 1
fi
echo "Take these files to your legal expert in copyright law:"
for L in $@
do
	for f in legalnotice-*-en_US.xml
	do
		initFile $f
	done
	for f in fedora-entities-en_US.ent
	do
		initFile $f
	done
done
