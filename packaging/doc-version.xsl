<!-- 

Extract version number for latest revision from rpm-info.xml.
Optional stringparam "role" allows querying of appropriate revision type.

-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" indent="no" method="text"/>

  <xsl:param name="role" select="'doc'"/>

  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="$role='rpm'">
	<xsl:choose>
	  <xsl:when test="/rpm-info/changelog/revision[1]/@role='doc'">
	    <xsl:text>0</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of
	      select="/rpm-info/changelog/revision[@role=$role][1]/@number"/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of
	  select="/rpm-info/changelog/revision[@role=$role][1]/@number"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
