# This spec file is only for building supporting common files from the
# docs-common module.  It is not for use with other docs modules at
# this time.
%define docs_common	$RPM_BUILD_ROOT%{_datadir}/fedora/doc/docs-common

Summary:        Fedora Documentation common files
Name:           fedora-doc-common
# To be defined in Makefile
Version:        0.4.1
Release: 	1
License:        Distributable
Url:		http://fedora.redhat.com/projects/docs/
Source0:	fedora-doc-common-%version.src.tar.gz
Group:          Documentation
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
This package contains the following official Fedora Documentation components:
- Common entities and XML fragments
- Desktop menu integration
- Help system integration for GNOME and KDE


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
install -d -m 755 %{docs_common}/bin/
for i in xmlformat xmldiff copy-figs tidy-bowl fdpsh doctype db2rpm-info \
      	 move-if-change
do
	install -m 755 bin/${i} \
		%{docs_common}/bin/
done
for i in xmlformat-fdp.conf xmldiff.pl active fdp-functions
do
	install -m 644 bin/${i} \
		%{docs_common}/bin/
done
install -d -m 755 %{docs_common}/common/entities
install -m 644 common/*.{xml,ent} %{docs_common}/common/
install -m 644 common/entities/* %{docs_common}/common/entities/
install -d -m 755 %{docs_common}/css/
install -m 644 css/* %{docs_common}/css/
install -d -m 755 $RPM_BUILD_ROOT/%{_sysconfdir}/xdg/menus/applications-merged/
install -m 644 packaging/fedora-documentation.menu \
	$RPM_BUILD_ROOT/%{_sysconfdir}/xdg/menus/applications-merged/
install -d -m 755 %{docs_common}/stylesheet-images/
install -m 644 stylesheet-images/* %{docs_common}/stylesheet-images/
install -d -m 755 %{docs_common}/packaging/
install -m 644 packaging/*.{xsl,dtd} %{docs_common}/packaging/
install -d -m 755 %{docs_common}/w3.org/2001/
install -m 644 w3.org/2001/* %{docs_common}/w3.org/2001/
install -d -m 755 %{docs_common}/xsl/
install -m 644 xsl/* %{docs_common}/xsl/
install -d -m 755 %{docs_common}/images/
install -m 644 images/* %{docs_common}/images/
install -d -m 755 $RPM_BUILD_ROOT/%{_datadir}/apps/khelpcenter/plugins/Fedora
install -m 644 packaging/khelpcenter-fdp.directory \
	$RPM_BUILD_ROOT/%{_datadir}/apps/khelpcenter/plugins/Fedora/.directory
install -m 644 Makefile.common %{docs_common}/Makefile.common


%clean 
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-, root, root,-)
%{_datadir}/fedora/
%{_datadir}/apps/khelpcenter/
%{_sysconfdir}/xdg/menus/applications-merged/*

%changelog
* Tue Apr 11 2006 Paul W. Frields <stickster@gmail.com> - 0.4.1-1
- Fix building routines to accommodate new entity parsing functions

* Sat Mar 11 2006 Paul W. Frields <stickster@gmail.com> - 0.4.0-1
- Updated build processes for proper locale generation

* Tue Feb 14 2006 Paul W. Frields <stickster@gmail.com> - 0.3.2-1
- Accommodate users' local %_build_name_fmt
- Improve readability with macro in spec file

* Mon Feb 13 2006 Paul W. Frields <stickster@gmail.com> - 0.3.1-1
- Update Makefile to properly remove supporting source files
- Support generation of fdp-info in local build environment
- Include additional scripts
- Revert spec.xsl to distinguish local builds from CVS builds

* Sun Feb 12 2006 Paul W. Frields <stickster@gmail.com> - 0.3-1
- Move to XInclude for all legalnotice content

* Wed Feb  8 2006 Paul W. Frields <stickster@gmail.com> - 0.2.1-1
- Separate out package building processes

* Fri Feb  3 2006 Paul W. Frields <stickster@gmail.com> - 0.2-1
- Initial RPM version

