function trim( s,	v )	{
	v = s
	sub( /^[ \t]*/, "", v )
	sub( /[ \t]*$/, "", v )
	return( v )
}
BEGIN	{
	prefix="/usr/share/doc/fedora-release-5/RELEASE-NOTES-";
	suffix=".html";
	nLangs = split( LANGS, langs );
	for( i = 1; i <= nLangs; ++i )	{
		lang = langs[ i ]
		loc = substr( lang, 1, 2 )
		shorts[ loc ] = lang
	}
	pri_lang = langs[ 1 ]
	# Read in the maps
	while( getline < MAP > 0 )	{
		sub( /#.*$/, "" )
		if( NF >= 2 )	{
			lang = $1
			pseudo = $2
			$1 = ""
			$2 = ""
			note = trim( $0 )
			pseudo2lang[ pseudo ] = lang
			notes[ lang ] = note
			printf "<!-- %s|%s|%s -->\n", lang, pseudo, note
		}
	}
}
# Perform our macro substitutions
{
	gsub( /<prefix\/>/, prefix )
	gsub( /<suffix\/>/, suffix )
	gsub( /<pri_lang\/>/, pri_lang )
}
# Fill in matches for our known ${LANGUAGES}
/<LANGS\/>/	{
	printf "\t// Attempt to match supported locales\n"
	for( i = 1; i <= nLangs; ++i )	{
		lang = langs[ i ]
		printf "\t<!-- %s:%s -->\n", lang, notes[ lang ]
		printf "\tcase \"%s\":\n", lang
		printf "\t\tbreak;\n"
	}
	next
}
# If ${LANGUAGES} matching fails, try to match the short form
/<REMAP\/>/	{
	printf "\t\t\t// Attempt to match generic locales\n"
	for( lang in pseudo2lang )	{
		found = 0
		for( i = 1; i <= nLangs; ++i )	{
			if( lang == langs[i] )	{
				found = 1
				break
			}
		}
		if( !found )	{
			printf "\t\t\tcase \"%s\":\n", lang
			printf "\t\t\t\t//* %s;\n", notes[ lang ]
			printf "\t\t\t\tmiddle = \"%s\";\n", pseudo2lang[ lang ]
			printf "\t\t\t\tbreak;\n"
		}
	}
	next
}
/<ITEMS\/>/	{
	for( i = 1; i <= nLangs; ++i )	{
		lang = langs[ i ]
		print "<DT><PARA>"
		printf "%s - ", lang
		if( lang in notes )	{
			print notes[ lang ]
		}
		if( lang in pseudo2lang )	{
			lang = pseudo2lang[ lang ]
		}
		url = prefix lang suffix
		print "</PARA></DT>"
		print "<DD>"
		printf "\t<a href=\"%s\">%s</a>\n", url, url
		print "</DD>"
	}
	next
}
{
	print
}
