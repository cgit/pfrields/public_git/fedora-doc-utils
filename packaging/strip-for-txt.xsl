<!-- Strip <revhistory> element from DocBook -->
<!-- Note this XSLT upgrades your DocBook to the V4.4 DTD! -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" indent="no" method="xml"
  omit-xml-declaration="no" standalone="no" version="1.0"
  doctype-public="-//OASIS//DTD DocBook XML V4.4//EN"
  doctype-system="http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" />

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="/book">
    <xsl:element name="book">
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::bookinfo">
	    <xsl:call-template name="bookinfo"/>
	  </xsl:when>
	  <xsl:when test="self::index">
	    <xsl:text/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:copy-of select="."/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template match="/article">
    <xsl:element name="article">
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::articleinfo">
	    <xsl:call-template name="articleinfo"/>
	  </xsl:when>
	  <xsl:when test="self::index">
	    <xsl:text/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:copy-of select="."/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template name="bookinfo">
    <xsl:element name="bookinfo">
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::revhistory">
	    <xsl:text/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:copy-of select="."/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template name="articleinfo">
    <xsl:element name="articleinfo">
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::revhistory">
	    <xsl:text/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:copy-of select="."/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
