<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Migrate rpm-info from old form to new form (ca. 2006-08) -->

  <xsl:param name="lang" select="'en_US'" />

  <xsl:output encoding="UTF-8" indent="yes" method="xml"
    omit-xml-declaration="no" version="1.0" standalone="no"
    doctype-system="../../docs-common/packaging/rpm-info.dtd" />

  <xsl:template match="/rpm-info">
    <xsl:element name="rpm-info">
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="name(.)='titles'">
	    <xsl:call-template name="titletemplate" />
	  </xsl:when>
	  <xsl:when test="name(.)='changelog'">
	    <xsl:call-template name="clogtemplate" />
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:copy-of select="." />
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template name="titletemplate">
    <xsl:element name="title">
      <xsl:value-of select="translation[@lang=$lang]/title" />
    </xsl:element>
    <xsl:element name="desc">
      <xsl:value-of select="translation[@lang=$lang]/desc" />
    </xsl:element>
  </xsl:template>

  <xsl:template name="clogtemplate">
    <xsl:element name="changelog">
      <xsl:copy-of select="@*" />
      <xsl:for-each select="*">
	<xsl:call-template name="revtemplate" />
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template name="revtemplate">
    <xsl:element name="revision">
      <xsl:copy-of select="@*" />
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="name(.)='details'">
	    <xsl:if test="@lang=$lang">
	      <xsl:call-template name="detailtemplate" />
	    </xsl:if>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:copy-of select="." />
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template name="detailtemplate">
    <xsl:element name="details">
      <xsl:value-of select="." />
    </xsl:element>
  </xsl:template>

  <xsl:attribute-set name="wipe" />
</xsl:stylesheet>
