<!-- 

Add a RPM changelog entry to a document's rpm-info.xml file.
The following stringparam values are expected in this file:
  detail : Text describing the change
    date : Date of change, formatted properly:
             For role="rpm", date format is +"%a %b %d %Y"
             For role="doc", date format is +"%Y-%m-%d"
  number : Number for change, formatted properly:
             For role="rpm", integer release number
             For role="doc", version number for document
  person : ID for responsible entity, drawn from current <colophon>
    role : "doc" or "rpm", indicating type of change

-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="fdpcommondir" select="'..'"/>

  <xsl:output encoding="UTF-8" indent="yes" method="xml"
    omit-xml-declaration="no" standalone="no" version="1.0"
    doctype-system="{$fdpcommondir}/packaging/rpm-info.dtd"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="rpm-info">
    <xsl:element name="rpm-info">
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::changelog"><xsl:call-template name="clog"/></xsl:when>
	  <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template name="clog">
    <xsl:element name="changelog" use-attribute-sets="clog">
      <xsl:element name="revision" use-attribute-sets="rev">
	<xsl:element name="author" use-attribute-sets="auth"/>
	<xsl:element name="details"><xsl:value-of
	select="$detail"/></xsl:element>
      </xsl:element>
      <xsl:copy-of select="*"/>
    </xsl:element>
  </xsl:template>


  <xsl:attribute-set name="clog">
    <xsl:attribute name="order">newest-first</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="rev">
    <xsl:attribute name="date"><xsl:value-of select="$date"/></xsl:attribute>
    <xsl:attribute name="number"><xsl:value-of
    select="$number"/></xsl:attribute>
    <xsl:attribute name="role"><xsl:value-of select="$role"/></xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="auth">
    <xsl:attribute name="worker"><xsl:value-of
    select="$person"/></xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
