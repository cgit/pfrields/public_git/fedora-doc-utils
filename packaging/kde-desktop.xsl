<!-- Transform rpm-info.xml into a .desktop file for KDE -->
<xsl:stylesheet version="1.0" xml:space="preserve" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" indent="no" method="text" omit-xml-declaration="yes" standalone="no" version="1.0"/>

<!-- Note: do not indent this file!  Any whitespace here
     will be reproduced in the output -->

  <xsl:param name="lang" select="'en_US'" />
  <xsl:param name="docbase" select="'example-tutorial'" />

<xsl:template match="/">[Desktop Entry]
Encoding=UTF-8
Categories=X-Fedora-Documentation;
Name=<xsl:value-of select="/rpm-info/titles/translation[@lang='en_US']/title"/>
Exec=khelpcenter help:/fedora-doc-<xsl:value-of
      select="$docbase"/>/index.html

<xsl:for-each select="/rpm-info/titles/translation"><xsl:if test="@lang != 'en_US'">Name[<xsl:value-of
	  select="@lang"/>]=<xsl:apply-templates
	  select="title"/></xsl:if></xsl:for-each>

<xsl:for-each select="/rpm-info/titles/translation"><xsl:if test="@lang !=
	'en_US'">Comment[<xsl:value-of select="@lang"/>]=<xsl:apply-templates
	select="desc"/></xsl:if></xsl:for-each>

Icon=icon-documentation.png
Terminal=false
Keywords=fedora
OnlyShowIn=KDE;
</xsl:template>

<xsl:include href="templates.xsl"/>

</xsl:stylesheet>
