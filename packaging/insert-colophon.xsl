<!-- 

Add a worker to a document's colophon. The following stringparam values are
required and expected:
  firstname : Contributor's first name
    surname : Contributor's family or surname
   initials : Initials for use in document revision history
      email : email address (should match Fedora Project email)

The following stringparam values are optional:
  othername : Middle initial

The following stringparam values will be set in this stylesheet:
  wholename : firstname + " " [ + othername + " " ] + surname
         id : firstname + surname

-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output encoding="UTF-8" indent="yes" method="xml"
    omit-xml-declaration="no" standalone="no" version="1.0"
    doctype-system="../../docs-common/packaging/rpm-info.dtd"/>

  <!-- Cope if no othername is provided -->
  <xsl:param name="othername" select="''"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="rpm-info">
    <xsl:element name="rpm-info">
      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::colophon">
	    <xsl:call-template name="colophon">
	      <xsl:with-param name="firstname"><xsl:value-of
		  select="$firstname"/></xsl:with-param>
	    </xsl:call-template>
	  </xsl:when>
	  <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template name="colophon">
    <xsl:element name="colophon">
      <xsl:copy-of select="*"/>
      <xsl:element name="worker" use-attribute-sets="person"/>
    </xsl:element>
  </xsl:template>

  <xsl:attribute-set name="person">
    <xsl:attribute name="firstname"><xsl:value-of
	select="$firstname"/></xsl:attribute>
    <xsl:attribute name="othername"><xsl:value-of
	select="$othername"/></xsl:attribute>
    <xsl:attribute name="surname"><xsl:value-of
	select="$surname"/></xsl:attribute>
    <xsl:attribute name="initials"><xsl:value-of
	select="$initials"/></xsl:attribute>
    <xsl:attribute name="email"><xsl:value-of select="$email"/></xsl:attribute>
    <xsl:attribute name="wholename"><xsl:value-of
	select="$firstname"/><xsl:text> </xsl:text><xsl:if
	test="$othername"><xsl:value-of 
	  select="$othername"/><xsl:text> </xsl:text></xsl:if><xsl:value-of 
	select="$surname"/></xsl:attribute>
    <xsl:attribute name="id"><xsl:value-of select="$firstname"/><xsl:value-of
	select="$surname"/></xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
