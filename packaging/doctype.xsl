<?xml version="1.0" encoding="UTF-8"?>

<!--
	This stylesheet will output the word "articleinfo" or
	"bookinfo" by finding either the <article> or <book> XML 
	element in the document.  Use it like this:

	xsltproc doctype.xsl file.xml

	The "file.xml" file MUST VALIDATE in order for the results
	to be meaningful.  If the document might not validate, use the
	"doctype" shell script instead.
  -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:template match="/article">articleinfo</xsl:template>
  <xsl:template match="/book">bookinfo</xsl:template>
  
</xsl:stylesheet>
