<!-- Get a list of all workers (full name and email address) -->

<xsl:stylesheet version="1.0" xml:space="preserve" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" standalone="no" version="1.0"/>

  <xsl:template match="/">
    <xsl:for-each select="/rpm-info/colophon/worker"><xsl:value-of
	select="@email"/>:<xsl:value-of select="@wholename"/>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
