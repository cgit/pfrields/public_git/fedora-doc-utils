<!-- Transform bookinfo|articleinfo section into FDP's rpm-info -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" indent="yes" method="xml" standalone="no"
	  omit-xml-declaration="no" version="1.0" doctype-system="../docs-common/packaging/rpm-info.dtd"/>

  <xsl:template match="book|article">
    <xsl:apply-templates select="articleinfo|bookinfo"/>
  </xsl:template>

  <xsl:template match="bookinfo|articleinfo" name="info">
    <xsl:element name="rpm-info">
      <xsl:apply-templates select="authorgroup"/>
      <xsl:element name="author">
	<xsl:attribute name="worker">FIXME</xsl:attribute>
      </xsl:element>
      <xsl:comment>CHECK LICENSING BELOW</xsl:comment>
      <xsl:element name="license">
	<xsl:element name="rights">OPL</xsl:element>
	<xsl:element name="version">1.0</xsl:element>
      </xsl:element>
      <xsl:for-each select="copyright">
	<xsl:copy-of select="."/>
      </xsl:for-each>
      <!-- <xsl:element name="titles"> -->
	<xsl:apply-templates select="title"/>
      <!-- </xsl:element> -->
      <xsl:element name="changelog">
	<xsl:attribute name="order">
	  <xsl:text>newest-first</xsl:text>
	</xsl:attribute>
	<xsl:apply-templates select="revhistory"/>
      </xsl:element>
    </xsl:element>
  </xsl:template>


  <xsl:template match="authorgroup">
    <xsl:element name="colophon">
      <xsl:comment>PLEASE ADD EMAIL ADDRESSES AND ENSURE</xsl:comment>
      <xsl:comment>"id" ATTRIBUTES MATCH "worker" ATTRIBUTES</xsl:comment>
      <xsl:comment>IN "changelog/author ELEMENTS.</xsl:comment>
      <xsl:comment>Only author|editor|othercredit processed.</xsl:comment>
      <xsl:comment>You may remove these comments after editing.</xsl:comment>
      <xsl:apply-templates select="author"/>
      <xsl:apply-templates select="editor"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="author|editor|othercredit">
    <xsl:call-template name="worker"/>
  </xsl:template>

  <xsl:template name="worker">
    <xsl:element name="worker" use-attribute-sets="worker-atts"/>
  </xsl:template>

  <xsl:attribute-set name="worker-atts">
    <xsl:attribute name="surname"><xsl:value-of	select="surname"/></xsl:attribute>
    <xsl:attribute name="firstname"><xsl:value-of select="firstname"/></xsl:attribute>
    <xsl:attribute name="othername"><xsl:value-of select="othername"/></xsl:attribute>
    <xsl:attribute name="id"><xsl:value-of select="firstname"/><xsl:value-of select="surname"/></xsl:attribute>
    <xsl:attribute name="email"><xsl:if	test="email"><xsl:value-of select="email"/></xsl:if></xsl:attribute>
    <xsl:attribute name="wholename"><xsl:value-of select="firstname"/><xsl:if test="othername"><xsl:text> </xsl:text><xsl:value-of select="othername"/></xsl:if><xsl:text> </xsl:text><xsl:value-of select="surname"/></xsl:attribute>
    <xsl:attribute name="initials"><xsl:value-of select="firstname"/><xsl:value-of select="surname"/></xsl:attribute>
  </xsl:attribute-set>


  <xsl:template match="title">
    <!-- <xsl:element name="translation" use-attribute-sets="translation-atts"> -->
      <xsl:call-template name="translation"/>
    <!-- </xsl:element> -->
    <!-- <xsl:comment>PLEASE ADD A TRANSLATION FOR EACH ADDITIONAL
      LANGUAGE.</xsl:comment> -->
  </xsl:template>

  <xsl:template name="translation">
    <xsl:element name="title"><xsl:value-of select="."/></xsl:element>
    <xsl:element name="desc"><xsl:comment>PLEASE ADD BOOK/ARTICLE DESCRIPTION TO RPM-INFO</xsl:comment></xsl:element>
    <!-- Sorry, can't langify out of separate docs right now. -->
  </xsl:template>

  <xsl:attribute-set name="translation-atts">
    <xsl:attribute name="lang"><xsl:value-of select="../../@lang"/></xsl:attribute>
  </xsl:attribute-set>


  <xsl:template match="revhistory">
    <xsl:apply-templates select="revision"><xsl:sort select="date" order="descending"/><xsl:sort select="revnumber" order="descending"/></xsl:apply-templates>
  </xsl:template>

  <xsl:template match="revision">
    <xsl:element name="revision" use-attribute-sets="revision-atts">
      <xsl:element name="author" use-attribute-sets="revision-worker-atts"/>
      <xsl:for-each select="revdescription/para">
	<xsl:element name="details" use-attribute-sets="revision-detail-atts">
	  <xsl:value-of select="."/>
	</xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:attribute-set name="revision-atts">
    <xsl:attribute name="date"><xsl:value-of select="date"/></xsl:attribute>
    <xsl:attribute name="number"><xsl:value-of select="revnumber"/></xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="revision-worker-atts">
    <xsl:attribute name="worker"><xsl:value-of select="authorinitials"/></xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="revision-detail-atts">
    <xsl:attribute name="lang"><xsl:value-of select="../../../../../@lang"/></xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>


<!--
Local variables:
mode: xml
End:
-->
