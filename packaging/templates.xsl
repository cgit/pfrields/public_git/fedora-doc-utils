<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:strip-space elements="*"/>

  <!-- Print the translated title of a document for language $lang -->
  <xsl:template name="get-title">
    <xsl:value-of select="/rpm-info/title"/>
  </xsl:template>

  <!-- Print the translated description of a document for language $lang -->
  <xsl:template name="get-desc">
    <xsl:value-of select="/rpm-info/desc"/>
  </xsl:template>

  <!-- Print a hierarchical entry for a given worker with id $who -->    
  <xsl:template name="human">
    <xsl:param name="who" select="''"/>
    <xsl:for-each select="/rpm-info/colophon/worker">
      <xsl:if test="@id = $who">
	<xsl:element name="surname">
	  <xsl:value-of select="@surname"/>
	</xsl:element>
	<xsl:element name="firstname">
	  <xsl:value-of select="@firstname"/>
	</xsl:element>
	<xsl:if test="@othername != ''">
	  <xsl:element name="othername">
	    <xsl:value-of select="@othername"/>
	  </xsl:element>
	</xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <!-- Output an RPM-compliant license snippet -->
  <xsl:template name="rpm-license"><xsl:choose><xsl:when
  test="$license = 'GNU FDL'"><xsl:text>FDL</xsl:text></xsl:when><xsl:when
  test="$license = 'OPL'"><xsl:text>OPL</xsl:text></xsl:when><xsl:otherwise><xsl:text>Distributable</xsl:text></xsl:otherwise></xsl:choose></xsl:template>
  
  <!-- Print a full name for a given worker with id $who -->
  <!-- DO NOT CHANGE SPACING! -->
  <xsl:template name="personname"><xsl:for-each
  select="/rpm-info/colophon/worker"><xsl:if test="@id = $who"><xsl:value-of
  select="@firstname"/> <xsl:if test="@othername != ''"><xsl:value-of
  select="@othername"/> </xsl:if><xsl:value-of
  select="@surname"/></xsl:if></xsl:for-each></xsl:template>

  <!-- Print a full name and email for a given worker with id $who -->
  <!-- DO NOT CHANGE SPACING! -->
  <xsl:template name="packager"><xsl:call-template
  name="personname"><xsl:with-param name="who"
  select="$who"/></xsl:call-template> &lt;<xsl:for-each
  select="/rpm-info/colophon/worker"><xsl:if test="@id = $who"><xsl:value-of
  select="@email"/></xsl:if></xsl:for-each>&gt;</xsl:template>

  <!-- Print initials for a given worker with id $who -->
  <!-- DO NOT CHANGE SPACING! -->
  <xsl:template name="initials">
    <xsl:param name="who" select="''"/>
    <xsl:for-each select="/rpm-info/colophon/worker">
      <xsl:if test="@id = $who">
	<xsl:value-of select="@initials"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
