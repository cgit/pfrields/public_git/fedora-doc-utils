<!-- Transform rpm-info.xml into a SPEC File -->
<xsl:stylesheet version="1.0" xml:space="preserve" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" indent="no" method="text" omit-xml-declaration="no" standalone="no" version="1.0"/>

<!-- Note: do not indent this file!  Any whitespace here
     will be reproduced in the output -->

  <xsl:param name="lang" select="'en_US'" />
  <xsl:param name="docbase" select="'example-tutorial'" />

<xsl:template match="/"># Fedora Documentation Specfile
%define	docbase	<xsl:value-of select="$docbase"/>
%{!?fdpdir:%define localbuild 1}
%{!?fdpdir:%define fdpdir %{_datadir}/fedora/doc}

Summary:	Fedora Documentation: %{docbase}
Name:		fedora-doc-%{docbase}
Version:	<xsl:value-of select="/rpm-info/changelog/revision[@role = 'doc'][1]/@number"/>
Release:	<xsl:value-of select="/rpm-info/changelog/revision[@role = 'rpm'][1]/@number"/>
License:	<xsl:call-template name="rpm-license"><xsl:with-param name='license' select='/rpm-info/license/rights'/></xsl:call-template>
Url:		http://fedora.redhat.com/projects/docs
Source0:	%{docbase}-%{version}.src.tar.gz
Source1:	%{name}-gnome.desktop
Source2: 	%{name}-kde.desktop
Source3:	%{name}-khelp.desktop
<xsl:for-each select="/rpm-info/titles/translation">Source<xsl:value-of
	select="position()+3" />:	%{name}-<xsl:choose><xsl:when
	  test="@lang = 'en_US'">C</xsl:when><xsl:otherwise><xsl:value-of
	    select="@lang"/></xsl:otherwise></xsl:choose>.omf
</xsl:for-each>
Group:		Documentation
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	scrollkeeper &gt;= 0.3.11
Requires:	fedora-doc-common
BuildRequires:	xmlto
BuildRequires:	kdelibs
# Used if builder does not specify common files locally
%{?localbuild:%{expand:BuildRequires: fedora-doc-common}}

%description
<xsl:value-of select="/rpm-info/titles/translation[@lang='en_US']/desc" />

<xsl:for-each select="/rpm-info/titles/translation"><xsl:if test="@lang != 'en_US'">%package        <xsl:value-of select="@lang"/>
Summary:        Fedora Documentation - <xsl:value-of select="title"/>
Group:          Documentation

%description	<xsl:value-of select="@lang"/>
<xsl:value-of select="desc"/></xsl:if>
</xsl:for-each>
%prep
%setup -q -n %{docbase}-%{version}


%build
export FDPDIR=%{fdpdir}
# Remove hard-coded FDPDIR locations from CVS docs
for i in $RPM_BUILD_DIR/%{docbase}-%{version}/{*.xml,*/*.xml,Makefile}
do
	%{__sed} -i 's;\(\.\./\)\{1,\}docs-common;%{fdpdir}/docs-common;g' "$i"
done
eval `make showvars | grep '\(PRI_LANG\|OTHERS\|DOC_ENTITIES\)'`
# Create entities (.ent files)
mkdir po/CVS	# This is to make the Makefile.common happy
for i in $PRI_LANG $OTHERS ; do
	make set-locale-$i
	for j in $DOC_ENTITIES ; do
		make ${i}/${j}.ent
	done
done
<xsl:for-each select="/rpm-info/titles/translation">
LANG=<xsl:value-of select="@lang"/>.UTF-8 xmllint --xinclude <xsl:value-of select="@lang"/>/%{docbase}.xml &gt; <xsl:value-of select="@lang"/>/%{docbase}.xml-parsed || :
# After XInclude, may need to scour FDPDIR misdirections again
%{__sed} 's;\(\.\./\)\{1,\}docs-common;%{fdpdir}/docs-common;g' <xsl:value-of select="@lang"/>/%{docbase}.xml-parsed &gt; <xsl:value-of select="@lang"/>/%{docbase}.xml
LANG=<xsl:value-of select="@lang"/>.UTF-8 xmlto -o %{docbase}-<xsl:value-of select="@lang"/> \
	-x %{fdpdir}/docs-common/xsl/main-html.xsl html \
	<xsl:value-of select="@lang"/>/%{docbase}.xml
make khelp
</xsl:for-each>

%install
for DIR in $RPM_BUILD_ROOT%{_datadir}/applications/kde \
	   $RPM_BUILD_ROOT%{_datadir}/apps/khelpcenter/plugins/Fedora \
	   $RPM_BUILD_ROOT%{_datadir}/omf/fedora-doc-%{docbase} \
	   <xsl:for-each select="/rpm-info/titles/translation">$RPM_BUILD_ROOT%{_docdir}/HTML/<xsl:choose><xsl:when test="@lang='en_US'">en</xsl:when><xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise></xsl:choose>/fedora-doc-%{docbase} \
	   $RPM_BUILD_ROOT%{_datadir}/fedora/doc/fedora-doc-%{docbase}/<xsl:choose><xsl:when test="@lang = 'en_US'">C</xsl:when><xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise></xsl:choose> \
	   </xsl:for-each>; do
	install -d -m 755 "$DIR"
done
export GLOBIGNORE=rpm-info.xml
desktop-file-install --vendor fedora \
		     --dir $RPM_BUILD_ROOT%{_datadir}/applications \
		     --add-category X-Fedora \
		     %{SOURCE1}
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_datadir}/applications/kde
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_datadir}/apps/khelpcenter/plugins/Fedora
<xsl:for-each select="/rpm-info/titles/translation">install -m 644 <xsl:value-of select="@lang"/>/*.{xml,ent} $RPM_BUILD_ROOT%{_datadir}/fedora/doc/fedora-doc-%{docbase}/<xsl:choose><xsl:when test="@lang='en_US'">C</xsl:when><xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise></xsl:choose>
install -m 644 %{SOURCE<xsl:value-of select="position()+3"/>} $RPM_BUILD_ROOT%{_datadir}/omf/fedora-doc-%{docbase}
<xsl:if test="@lang != 'en_US'">install -m 644 <xsl:value-of select="@lang"/>/*.{xml,ent} $RPM_BUILD_ROOT%{_datadir}/fedora/doc/fedora-doc-%{docbase}/<xsl:value-of select="@lang"/></xsl:if>
pushd %{docbase}-<xsl:value-of select="@lang"/>
find . -type d | xargs -i \
     install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/HTML/<xsl:choose><xsl:when test="@lang='en_US'">en</xsl:when><xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise></xsl:choose>/fedora-doc-%{docbase}/{}
find -L . -type f | xargs -i \
     install -D -m 644 {} $RPM_BUILD_ROOT%{_docdir}/HTML/<xsl:choose><xsl:when test="@lang='en_US'">en</xsl:when><xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise></xsl:choose>/fedora-doc-%{docbase}/{}
popd
</xsl:for-each>

%clean
/bin/rm -rf $RPM_BUILD_ROOT

<xsl:for-each select="/rpm-info/titles/translation">
%post	<xsl:if test="@lang != 'en_US'"><xsl:value-of select="@lang"/></xsl:if>
/usr/bin/scrollkeeper-update || true

%postun	<xsl:if test="@lang != 'en_US'"><xsl:value-of select="@lang"/></xsl:if>
/usr/bin/scrollkeeper-update || true
</xsl:for-each>

%files
%defattr(-, root, root, -)
%{_datadir}/fedora/doc/fedora-doc-%{docbase}/
%{_datadir}/applications/*.desktop
%{_datadir}/applications/kde/*.desktop
%{_datadir}/apps/khelpcenter/
%{_datadir}/omf/fedora-doc-%{docbase}/
%{_docdir}/HTML/en/fedora-doc-%{docbase}/

<xsl:for-each select="/rpm-info/titles/translation"><xsl:if test="@lang != 'en_US'">
%files	<xsl:value-of select="@lang"/>
%defattr(-, root, root, -)
%dir %{_datadir}/fedora/doc/fedora-doc-%{docbase}/<xsl:value-of select="@lang"/>
%{_datadir}/fedora/doc/fedora-doc-%{docbase}/<xsl:value-of select="@lang"/>/*
%{_datadir}/omf/fedora-doc-%{docbase}/fedora-doc-%{docbase}-<xsl:value-of select="@lang"/>.omf
%{_docdir}/HTML/<xsl:choose><xsl:when test="@lang='en_US'">en</xsl:when><xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise></xsl:choose>/fedora-doc-%{docbase}/*
</xsl:if></xsl:for-each>

%changelog
<xsl:for-each select="/rpm-info/changelog/revision"><xsl:choose><xsl:when 
	test="@role='rpm'">* <xsl:value-of select="@date"/> Fedora Docs Project &lt;fedora-docs-list@redhat.com&gt; - <xsl:value-of
	  select="following-sibling::revision[@role='doc']/@number"/>-<xsl:value-of select="@number"/>
- <xsl:value-of select="details"/>

</xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:for-each>
</xsl:template>

  <xsl:include href="templates.xsl"/>
</xsl:stylesheet>
