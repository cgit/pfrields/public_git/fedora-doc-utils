<!-- Transform rpm-info.xml into an OMF file -->
<xsl:stylesheet version="1.0" xml:space="preserve" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output doctype-public="-//OMF//DTD Scrollkeeper OMF Variant V1.0//EN" doctype-system="http://scrollkeeper.sourceforge.net/dtds/scrollkeeper-omf-1.0/scrollkeeper-omf.dtd" encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="no" standalone="no" version="1.0"/>

<!-- Note: do not indent this file!  Any whitespace here
     will be reproduced in the output -->

  <xsl:param name="lang" select="'en_US'" />
  <xsl:param name="docbase" select="'example-tutorial'" />

  <xsl:template match="/">
    <omf>
      <resource>
        <creator>
          fedora-docs-list@redhat.com (Fedora Documentation Project)
        </creator>
        <maintainer>
          fedora-docs-list@redhat.com (Fedora Documentation Project)
        </maintainer>
        <title>
          <xsl:call-template name="get-title"/>
        </title>
        <date>
          <xsl:value-of select="/rpm-info/changelog/revision[1]/@date"/>
        </date>
        <version identifier="{/rpm-info/changelog/revision[@role = 'doc'][1]/@number}" date="{/rpm-info/changelog/revision[@role = 'doc'][1]/@date}" description="{$docbase}-{/rpm-info/changelog/revision[@role = 'doc'][1]/@number} ({/rpm-info/changelog/revision[@role = 'doc'][1]/@date})"
        />
        <subject category="General|Linux|Distributions|Other"/>
        <description>
          <xsl:call-template name="get-desc"/>
        </description>
        <type>
          <xsl:call-template name="get-title"/>
        </type>
        <format mime="text/xml" dtd="-//OASIS//DTD DocBook XML V4.2//EN"/>
        <xsl:choose>
	  <xsl:when test="$lang='en_US'">
	    <identifier url="file:/usr/share/fedora/doc/fedora-doc-{$docbase}/C/{$docbase}.xml"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <identifier url="file:/usr/share/fedora/doc/fedora-doc-{$docbase}/{$lang}/{$docbase}.xml"/>
	  </xsl:otherwise>
	</xsl:choose>
        <xsl:choose>
	  <xsl:when test="$lang='en_US'"><language code="C"/></xsl:when>
	  <xsl:otherwise><language code="$lang"/></xsl:otherwise>
	</xsl:choose>
        <relation seriesid="7a5d3ea2-2a1e-11da-86a3-8ea47a4bb227"/>
        <rights type="OPL" license.version="1.0" />
      </resource>
    </omf>
  </xsl:template>

  <xsl:include href="templates.xsl"/>

</xsl:stylesheet>
