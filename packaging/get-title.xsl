<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" preserve-space="no" encoding="UTF-8"
    indent="no" method="text"/>
  <xsl:template match="*">
    <xsl:value-of select="/rpm-info/title"/>
  </xsl:template>
</xsl:stylesheet>
