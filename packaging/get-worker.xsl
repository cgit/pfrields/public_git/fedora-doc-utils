<!-- 

Get a worker attribute by name for a specific worker.
  pos : param, integer for position (default: 1)
  att : stringparam, name of attribute to fetch (default: id)
N.B.: xml:space attribute not set to avoid output ugliness

-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" standalone="no" version="1.0"/>

  <xsl:param name="pos" select="1"/>
  <xsl:param name="att" select="'id'"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:for-each select="/rpm-info/colophon/worker[$pos]">
      <xsl:choose>
	<xsl:when test="$att='email'"><xsl:value-of select="@email"/></xsl:when>
	<xsl:when test="$att='wholename'"><xsl:value-of
	    select="@wholename"/></xsl:when>
	<xsl:when test="$att='id'"><xsl:value-of select="@id"/></xsl:when>
	<xsl:otherwise><xsl:text>ERROR</xsl:text></xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
