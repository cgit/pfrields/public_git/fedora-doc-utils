<!-- Transform rpm-info.xml into a KHelpCenter .desktop File -->
<xsl:stylesheet version="1.0" xml:space="preserve" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" indent="no" method="text" omit-xml-declaration="yes" standalone="no" version="1.0"/>

<!-- Note: do not indent this file!  Any whitespace here
     will be reproduced in the output -->

  <xsl:param name="lang" select="'en_US'" />
  <xsl:param name="docbase" select="'example-tutorial'" />

<xsl:template match="/">[Desktop Entry]
DocPath=help:/fedora-doc-<xsl:value-of select="$docbase"/>/index.html
Name=<xsl:value-of select="/rpm-info/titles/translation[@lang='en_US']/title"/>

<xsl:for-each select="/rpm-info/titles/translation">Name[<xsl:value-of
	select="@lang"/>]=<xsl:value-of select="title"/>
</xsl:for-each>
X-DOC-Weight=-5000
</xsl:template>

<xsl:include href="templates.xsl"/>
</xsl:stylesheet>
