###############################################################################
# Makefile for fedora-doc-common packaging
# Created by: Paul W. Frields <stickster@gmail.com>
# License: GPL
# Copyright 2005 Paul W. Frields
###############################################################################

DOCBASE = fedora-doc-common

######################################################
# Some packaging specific vars
DATE		= $(shell date +"%Y-%m-%d")
NOW 		= $(shell date +"%a %b %e %Y")
SPECIN		= ../docs-common/packaging/$(DOCBASE).spec
VERSION		= $(shell rpm -q --specfile --queryformat '%{version}\n' $(SPECIN))
RELEASE		= $(shell rpm -q --specfile --queryformat '%{release}\n' $(SPECIN))
DOCSPEC 	= $(shell rpm ${RPMFLAGS} --eval '%{_specdir}')/$(DOCBASE).spec
DOCSRCTAR 	= $(shell rpm ${RPMFLAGS} --eval '%{_sourcedir}')/$(DOCBASE)-$(VERSION).src.tar.gz
TITLE		= Fedora Documentation Project Shared Files
######################################################
# Some RPM flags...
######################################################
RPMFLAGS 	= 	--define "docbase $(DOCBASE)" \
			--define "_topdir $(PWD)/rpmbuild" \
			--define "_build_name_fmt %{name}-%{version}-%{release}.%{arch}.rpm"

RPM		= rpm ${RPMFLAGS}
######################################################


clean:
	rm -rf $(DOCBASE)*.rpm
	rm -rf $(PWD)/rpmbuild

update: clean
	cvs update

rpm: update
#
# Make RPM-compliant tarball of source XML and other stuff
	mkdir $(DOCBASE)-$(VERSION)
	find . -maxdepth 1 -type d \( \
		-name 'bin' -o \
		-name 'common' -o \
		-name 'css' -o \
		-name 'images' -o \
		-name 'packaging' -o \
		-name 'stylesheet-images' -o \
		-name 'xsl' \) | xargs -i \
		find {} -maxdepth 1 -type f ! \( \
		-name '.*' -o -name '*~' -o \
		-name '*.svg' -o -name '*.eps' \) | \
		cpio -pamdv $(DOCBASE)-$(VERSION)
	find . -maxdepth 2 -type d \
		-name 'entities' -o \
		-name '2001' | xargs -i \
		find {} -maxdepth 1 -type f | \
		cpio -pamdv $(DOCBASE)-$(VERSION)
# include Makefile.common for use in offline FDP doc building
	cp Makefile.common $(DOCBASE)-$(VERSION)
#
# Make RPM build tree; don't rely on local user's setup
	mkdir -p rpmbuild/{BUILD,RPMS/noarch,SOURCES,SPECS,SRPMS}
	tar -zcvf $(DOCSRCTAR) $(DOCBASE)-$(VERSION)
	cp $(SPECIN) $(DOCSPEC)
	rm -rf $(DOCBASE)-$(VERSION)/
#FORGET THIS CRAP... replace with 'make clog' shortly
# Make rpmlint happy with a changelog entry
# FIXME: Maybe more magic would make this stickier; pity
#        I'm no magician...
#	sed 's/\(%changelog\)/\1\n* $(NOW) Fedora Docs Project <fedora-docs-list@redhat.com> - $(VERSION)-$(RELEASE)\n- Update to $(RELEASE)\n/' \
#		$(SPECIN) > $(DOCSPEC)
#
# Do the build...
#
	rpmbuild -ba $(RPMFLAGS) $(DOCSPEC)
	mv $(shell ${RPM} --eval '%{_rpmdir}')/*.rpm .
	mv $(shell ${RPM} --eval '%{_srcrpmdir}')/*.rpm .
	rpmbuild --clean --rmsource $(RPMFLAGS) $(DOCSPEC)
	rm -rf $(shell ${RPM} --eval '%{_topdir}')
